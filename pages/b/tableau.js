import axios from 'axios'
import Konva from 'konva/lib/Core'
import pell from 'pell'
import linkifyHtml from 'linkify-html'
import stripTags from 'voca/strip_tags'
import ClipboardJS from 'clipboard'
import fileSaver from 'file-saver'
const { saveAs } = fileSaver
import html2canvas from 'html2canvas'
import dayjs from 'dayjs'
import 'dayjs/locale/fr'
import 'dayjs/locale/it'
import localizedFormat from 'dayjs/plugin/localizedFormat'
import relativeTime from 'dayjs/plugin/relativeTime'
import ChargementPage from '#root/components/chargement-page.vue'
import Chargement from '#root/components/chargement.vue'
import Message from '#root/components/message.vue'
import Notification from '#root/components/notification.vue'
import Emojis from '#root/components/emojis.vue'
import { VueDraggableNext } from 'vue-draggable-next'
dayjs.extend(localizedFormat)
dayjs.extend(relativeTime)

export default {
	name: 'Tableau',
	components: {
		ChargementPage,
		Chargement,
		Message,
		Notification,
		Emojis,
		draggable: VueDraggableNext
	},
	data () {
		return {
			chargementPage: true,
			chargement: false,
			message: '',
			notification: '',
			modale: '',
			menu: '',
			question: '',
			questions: ['motPrefere', 'filmPrefere', 'chansonPreferee', 'prenomMere', 'prenomPere', 'nomRue', 'nomEmployeur', 'nomAnimal'],
			reponse: '',
			nouvellequestion: '',
			nouvellereponse: '',
			largeur: 1920,
			hauteur: 1080,
			echelle: 1,
			items: [[]],
			texte: '',
			image: { html: '', fichier: '' },
			commentaire: '',
			commentaireModifie: '',
			editeur: '',
			emojis: '',
			commentairesItem: [],
			editionCommentaire: false,
			itemId: '',
			commentaireId : '',
			commentairesPage: {},
			accordeonOuvert: '',
			nom: '',
			objet: '',
			objetVerrouille: false,
			outilSelectionner: true,
			outilDessiner: false,
			dessin: false,
			itemsDessin: [],
			historique: [],
			positionHistorique: 0,
			positionActuelleHistorique: 0,
			selection: false,
			positionX1: 0,
			positionY1: 0,
			positionX2: 0,
			positionY2: 0,
			positionSelectionX: 0,
			positionSelectionY: 0,
			largeurSelection: 0,
			hauteurSelection: 0,
			positionStylo: [],
			couleurStylo: '#000000',
			couleurSelecteur: '#000000',
			epaisseurStylo: 2,
			transparenceStylo: 1,
			outil: '',
			creation: false,
			positionObjetX: 0,
			positionObjetY: 0,
			largeurObjet: 0,
			hauteurObjet: 0,
			utilisateurs: [],
			page: 0,
			chrono: '',
			itemsDupliques: [],
			itemsSelectionnes: [],
			imagesASupprimer: [],
			itemCopie: {},
			codeqr: '',
			nouveaunom: '',
			donneesRoleUtilisateur: { identifiant: '', role: '', message: '' },
			fonds: ['points.png', 'lignes-horizontales.png', 'quadrillage.png', 'quadrillage-gris.png', 'quadrillage-grand.png', 'quadrillage-gris-grand.png', 'couleur-ffffff.png', 'couleur-00ced1.png', 'couleur-00a885.png', 'couleur-f7d000.png', 'couleur-ff2d55.png'],
			ctrl: false,
			requete: '',
			resultats: {},
			pageRecherche: 1,
			elementPrecedent: null,
			hote: this.$pageContext.pageProps.hote,
			userAgent: this.$pageContext.pageProps.userAgent,
			identifiant: this.$pageContext.pageProps.identifiant,
			nomUtilisateur: this.$pageContext.pageProps.nom,
			admin: this.$pageContext.pageProps.admin,
			role: this.$pageContext.pageProps.role,
			tableaux: this.$pageContext.pageProps.tableaux,
			langues: this.$pageContext.pageProps.langues,
			langue: this.$pageContext.pageProps.langue,
			tableau: this.$pageContext.pageProps.tableau,
			titre: this.$pageContext.pageProps.titre,
			donnees: this.$pageContext.pageProps.donnees,
			options: this.$pageContext.pageProps.options,
			vues: this.$pageContext.pageProps.vues,
			digidrive: this.$pageContext.pageProps.digidrive,
			commentaires: this.$pageContext.pageProps.commentaires,
			limite: parseFloat(import.meta.env.VITE_UPLOAD_LIMIT),
			pixabayAPI: import.meta.env.VITE_PIXABAY_API
		}
	},
	computed: {
		viaDigidrive () {
			return this.role === 'auteur' && parseInt(this.digidrive) === 1
		}
	},
	watch: {
		outilSelectionner: function (valeur) {
			if (valeur === true) {
				this.activerDeplacement()
			}
			this.reinitialiserSelection()
		},
		outilDessiner: function (valeur) {
			if (valeur === true) {
				this.desactiverDeplacement()
				this.couleurStylo = '#000000'
				this.couleurSelecteur = '#000000'
			} else {
				this.couleurSelecteur = '#000000'
			}
			this.positionStylo = []
			this.reinitialiserSelection()
		},
		outil: function (valeur) {
			if (valeur === '') {
				this.activerDeplacement()
			} else {
				this.desactiverDeplacement()
			}
		},
		pageRecherche: function (page) {
			this.rechercher(page)
		},
		accordeonOuvert: function (itemId) {
			const accordeons = document.querySelectorAll('.contenu-accordeon')
			accordeons.forEach(function (accordeon) {
				accordeon.style.display = 'none'
			})
			if (itemId !== '') {
				const accordeon = document.querySelector('#accordeon_' + itemId + ' .contenu-accordeon')
				accordeon.style.display = 'block'
			}
		}
	},
	async created () {
		const params = this.$pageContext.pageProps.params
		const langue = params.lang
		if (langue && this.langues.includes(langue) === true) {
			this.$i18n.locale = langue
			this.langue = langue
			this.$socket.emit('modifierlangue', langue)
		} else {
			this.$i18n.locale = this.langue
		}
		const page = params.page
		const question = params.q
		const reponse = params.r
		if (question && question !== '' && reponse && reponse !== '') {
			window.history.replaceState({}, document.title, window.location.href.split('?')[0])
		}
		if (page && (page - 1) > -1 && (page - 1) < this.donnees.length) {
			this.page = page - 1
		}

		this.ecouterSocket()

		if (this.options.hasOwnProperty('multipage') === false) {
			this.options.multipage = 'non'
		}
		if (this.options.hasOwnProperty('edition') === false) {
			this.options.edition = 'ouverte'
		}
		if (this.options.hasOwnProperty('affichageAuteur') === false) {
			this.options.affichageAuteur = 'non'
		}

		if (this.limite === 0 || this.limite === null || this.limite === undefined) {
			this.limite = 2
		}

		const observer = new PerformanceObserver((liste) => {
			liste.getEntries().forEach(async function (entree) {
				if (entree.type === 'back_forward') {
					const reponse = await axios.post(this.hote + '/api/recuperer-donnees-tableau', {
						tableau: this.tableau
					}, {
						headers: { 'Content-Type': 'application/json' }
					})
					if (reponse && reponse.hasOwnProperty('data') && reponse.data !== 'erreur') {
						if (reponse.data.hasOwnProperty('statut') === true && reponse.data.statut === 'ferme') {
							reponse.data.options.edition = 'fermee'
						}
						this.titre = reponse.data.titre
						this.donnees = reponse.data.donnees
						this.options = reponse.data.options
						this.vues = reponse.data.vues
						this.digidrive = reponse.data.digidrive
						this.chargerItems(this.donnees, this.page)
					}
				}
			}.bind(this))
		})
		observer.observe({ type: 'navigation', buffered: true })

		this.$socket.emit('connexion', { tableau: this.tableau, identifiant: this.identifiant, nom: this.nomUtilisateur, role: this.role })
	},
	mounted () {
		if (this.role === 'auteur' && this.tableaux.includes(this.tableau)) {
			this.admin = true
		}

		document.getElementsByTagName('html')[0].setAttribute('lang', this.langue)

		this.largeurInitiale = document.querySelector('#tableau').offsetWidth
		this.definirDimensions()

		this.chargerItems(this.donnees, this.page)

		if (this.options.fond.substring(0, 1) === '#') {
			document.querySelector('#tableau .konvajs-content').style.backgroundColor = this.options.fond
			document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'none'
		} else {
			document.querySelector('#tableau .konvajs-content').style.backgroundColor = 'transparent'
			document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'url(/img/' + this.options.fond + ')'
			document.querySelector('#tableau .konvajs-content').style.backgroundRepeat = 'repeat'
		}

		const lien = this.hote + '/b/' + this.tableau
		const clipboardLien = new ClipboardJS('#copier-lien .lien', {
			text: function () {
				return lien
			}
		})
		clipboardLien.on('success', function () {
			document.querySelector('#copier-lien .lien').focus()
			this.notification = this.$t('lienCopie')
		}.bind(this))
		const iframe = '<iframe src="' + this.hote + '/b/' + this.tableau + '" frameborder="0" width="100%" height="500"></iframe>'
		const clipboardCode = new ClipboardJS('#copier-code span', {
			text: function () {
				return iframe
			}
		})
		clipboardCode.on('success', function () {
			document.querySelector('#copier-code span').focus()
			this.notification = this.$t('codeCopie')
		}.bind(this))

		window.addEventListener('resize', this.definirDimensions, false)

		if (this.admin || this.role === 'editeur' || this.options.edition === 'ouverte') {
			window.addEventListener('keydown', this.gererClavierAdmin, false)
		}

		window.addEventListener('keyup', function (event) {
			if (event.key === 'Control') {
				this.ctrl = false
			}
		}.bind(this), false)

		window.addEventListener('beforeunload', this.quitterPage, false)

		window.addEventListener('paste', function (event) {
			if (this.modale !== 'texte' && this.modale !== 'commentaire' && this.modale !== 'recherche') {
				this.coller(event)
			}
		}.bind(this), false)

		document.querySelector('#tableau').addEventListener('dragover', function (event) {
			event.preventDefault()
			event.stopPropagation()
		}, false)

		document.querySelector('#tableau').addEventListener('dragcenter', function (event) {
			event.preventDefault()
			event.stopPropagation()
		}, false)

		document.querySelector('#tableau').addEventListener('drop', function (event) {
			event.preventDefault()
			event.stopPropagation()
			if (event.dataTransfer.files && event.dataTransfer.files[0]) {
				this.televerserImage(event.dataTransfer.files[0], 'drop')
			}
		}.bind(this), false)

		document.addEventListener('keydown', this.gererClavier, false)
	},
	beforeUnmount () {
		document.removeEventListener('keydown', this.gererClavier, false)
	},
	methods: {
		definirDimensions () {
			this.$nextTick(function () {
				let largeur = document.body.clientWidth - 54
				let hauteur = largeur / (16 / 9)
				if (hauteur > (document.body.clientHeight - 54)) {
					hauteur = document.body.clientHeight - 54
					largeur = hauteur * (16 / 9)
				}
				this.echelle = largeur / this.largeur
				if (document.querySelector('#tableau .konvajs-content')) {
					document.querySelector('#tableau .konvajs-content').style.transform = 'scale(' + this.echelle + ')'
				}
				if (this.items[this.page] && this.items[this.page].length > 0) {
					this.$nextTick(function () {
						this.items[this.page].forEach(function (item) {
							if (item.objet === 'ancre') {
								item.width = 10 / this.echelle
								item.height = 10 / this.echelle
								item.radius = 10 / this.echelle
							}
						}.bind(this))
						this.transformer()
					}.bind(this))
				}
			}.bind(this))
		},
		copierLien () {
			document.querySelector('#copier-lien .lien').click()
		},
		copierIframe () {
			document.querySelector('#copier-code span').click()
		},
		afficherCodeQR () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'code-qr'
			this.$nextTick(function () {
				const lien = this.hote + '/b/' + this.tableau
				// eslint-disable-next-line
				this.codeqr = new QRCode('qr', {
					text: lien,
					width: 360,
					height: 360,
					colorDark: '#000000',
					colorLight: '#ffffff',
					// eslint-disable-next-line
					correctLevel : QRCode.CorrectLevel.H
				})
				document.querySelector('.modale .fermer').focus()
			}.bind(this))
		},
		fermerModale () {
			this.modale = ''
			this.gererFocus()
		},
		fermerMessage () {
			this.message = ''
			this.gererFocus()
		},
		definirElementPrecedent (element) {
			this.elementPrecedent = element
		},
		chargerItems (items, page) {
			const donnees = JSON.parse(JSON.stringify(items))
			const images = []
			for (let i = 0; i < donnees[page].length; i++) {
				if (donnees[page][i].objet === 'image' && donnees[page][i].fichier !== '') {
					const donneesImage = new Promise(function (resolve) {
						const image = new window.Image()
						if (this.verifierURL(donnees[page][i].fichier) === false) {
							image.src = '/fichiers/' + this.tableau + '/' + donnees[page][i].fichier
						} else {
							image.crossOrigin = 'Anonymous'
							image.src = donnees[page][i].fichier
						}
						image.onload = function () {
							donnees[page][i].image = image
							resolve('termine')
						}
						image.onerror = function () {
							resolve()
						}
					}.bind(this))
					images.push(donneesImage)
				}
			}
			Promise.all(images).then(function () {
				this.items = donnees
				this.$nextTick(function () {
					const transformer = this.$refs.transformer.getNode()
					const stage = transformer.getStage()
					const objets = stage.find((item) => {
						return item.getAttrs().objet && item.getAttrs().objet === 'image'
					})
					for (let i = 0; i < objets.length; i++) {
						if (objets[i].getAttrs().filtre === 'Aucun') {
							objets[i].filters([])
						} else {
							objets[i].cache()
							objets[i].filters([Konva.Filters[objets[i].getAttrs().filtre]])
							if (objets[i].getAttrs().filtre === 'Pixelate') {
								objets[i].pixelSize(10)
							}
						}
					}
					if (!this.admin && this.role !== 'editeur' && this.options.edition !== 'ouverte') {
						this.desactiverDeplacement()
					}
					this.transformer()
				}.bind(this))
				this.$nextTick(function () {
					this.items[this.page].forEach(function (item) {
						if (item.objet === 'ancre') {
							item.visible = false
							item.width = 10 / this.echelle
							item.height = 10 / this.echelle
							item.radius = 10 / this.echelle
						}
					}.bind(this))
					const itemsCommentaire = []
					const commentairesPage = {}
					this.items[this.page].forEach(function (item) {
						if (item.objet === 'commentaire') {
							itemsCommentaire.push(item.name)
						}
					})
					itemsCommentaire.forEach(function (item) {
						if (this.commentaires.hasOwnProperty(item) === true) {
							commentairesPage[item] = JSON.parse(JSON.stringify(this.commentaires[item]))
						}
					}.bind(this))
					this.commentairesPage = commentairesPage
					this.transformer()
				}.bind(this))
				setTimeout(function () {
					this.chargementPage = false
                    this.chargement = false
				}.bind(this), 150)
			}.bind(this))
		},
		definirOutilPrincipal (type) {
			this.outilSelectionner = false
			this.outilDessiner = false
			if (type === 'selectionner') {
				this.outilSelectionner = true
				this.desactiverSelecteur()
			} else if (type === 'dessiner') {
				this.outilDessiner = true
				this.activerSelecteur()
			}
		},
		selectionnerOutil (type) {
			const outils = document.querySelectorAll('#outils .outil.actif')
			let actif = ''
			outils.forEach(function (outil) {
				if (outil.id) {
					actif = outil.id
				}
			})
			this.definirOutilPrincipal('selectionner')
			this.reinitialiserOutils()
			if ('outil-' + type !== actif) {
				this.outil = type
			}
			if (type === 'recherche') {
				this.elementPrecedent = (document.activeElement || document.body)
				this.modale = 'recherche'
				this.$nextTick(function () {
					document.querySelector('#recherche input').focus()
				}.bind(this))
			}
		},
		selectionnerOutilImage () {
			this.selectionnerOutil('image')
			document.querySelector('#televerser-image').click()
		},
		ajouter (type) {
			this.definirOutilPrincipal('selectionner')
			this.objetVerrouille = false
			const id = (new Date()).getTime() + Math.random().toString(16).slice(10)
			let largeur, hauteur, ratio, items, nom, objet
			let total = 0
			if (type === 'image') {
				largeur = this.image.html.width
				hauteur = this.image.html.height
				ratio = largeur / hauteur
				if (hauteur > this.hauteur / 3) {
					hauteur = this.hauteur / 3
					largeur = hauteur * ratio
				}
				if (largeur > this.largeur / 3) {
					largeur = this.largeur / 3
					hauteur = largeur / ratio
				}
			} else if (type === 'commentaire') {
				this.items[this.page].forEach(function (item) {
					if (item.objet === 'commentaire') {
						total++
					}
				})
			}
			switch (type) {
			case 'rectangle':
				items = [{ name: 'rect' + id, objet: 'rectangle', x: this.positionObjetX, y: this.positionObjetY, width: this.largeurObjet, height: this.hauteurObjet, rotation: 0, fill: 'transparent', stroke: '#ff0000', strokeWidth: 3, strokeScaleEnabled: false, opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant }]
				nom = 'rect' + id
				objet = 'rectangle'
				break
			case 'rectangle-plein':
				items = [{ name: 'rect' + id, objet: 'rectangle-plein', x: this.positionObjetX, y: this.positionObjetY, width: this.largeurObjet, height: this.hauteurObjet, rotation: 0, fill: '#cccccc', opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant }]
				nom = 'rect' + id
				objet = 'rectangle-plein'
				break
			case 'cercle':
				items = [{ name: 'circ' + id, objet: 'cercle', x: this.positionObjetX + (this.largeurObjet / 2), y: this.positionObjetY + (this.hauteurObjet / 2), width: this.largeurObjet, height: this.hauteurObjet, rotation: 0, fill: 'transparent', stroke: '#ff0000', strokeWidth: 3, strokeScaleEnabled: false, opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant }]
				nom = 'circ' + id
				objet = 'cercle'
				break
			case 'cercle-plein':
				items = [{ name: 'circ' + id, objet: 'cercle-plein', x: this.positionObjetX + (this.largeurObjet / 2), y: this.positionObjetY + (this.hauteurObjet / 2), width: this.largeurObjet, height: this.hauteurObjet, rotation: 0, fill: '#cccccc', opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant }]
				nom = 'circ' + id
				objet = 'cercle-plein'
				break
			case 'etoile':
				items = [{ name: 'star' + id, objet: 'etoile', x: this.positionX1, y: this.positionY1, width: 120, height: 120, rotation: 0, fill: '#ffff00', stroke: 'black', strokeWidth: 3, strokeScaleEnabled: false, numPoints: 5, innerRadius: 20, outerRadius: 45, opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant }]
				nom = 'star' + id
				objet = 'etoile'
				break
			case 'surlignage':
				items = [{ name: 'rect' + id, objet: 'surlignage', x: this.positionObjetX, y: this.positionObjetY, width: this.largeurObjet, height: this.hauteurObjet, rotation: 0, fill: '#ffff00', opacity: 0.5, draggable: true, verrouille: false, identifiant: this.identifiant }]
				nom = 'rect' + id
				objet = 'surlignage'
				break
			case 'ligne':
				items = [{ name: 'line' + id, objet: 'ligne', x: 0, y: 0, points: [this.positionX1, this.positionY1, this.positionX2, this.positionY2], fill: '#ff0000', stroke: '#ff0000', strokeWidth: 3, hitStrokeWidth: 50, opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant }, { name: 'ancr_line' + id + '_1', objet: 'ancre', x: this.positionX1, y: this.positionY1, radius: 10 / this.echelle, width: 10 / this.echelle, height: 10 / this.echelle, fill: '#ffff00', stroke: '#000000', strokeWidth: 1, visible: true, opacity: 1, draggable: true }, { name: 'ancr_line' + id + '_2', objet: 'ancre', x: this.positionX2, y: this.positionY2, radius: 10 / this.echelle, width: 10 / this.echelle, height: 10 / this.echelle, fill: '#ffff00', stroke: '#000000', strokeWidth: 1, visible: true, opacity: 1, draggable: true }]
				nom = 'line' + id
				objet = 'ligne'
				break
			case 'fleche':
				items = [{ name: 'flec' + id, objet: 'fleche', x: 0, y: 0, points: [this.positionX1, this.positionY1, this.positionX2, this.positionY2], pointerLength: 25, pointerWidth: 20, fill: '#ff0000', stroke: '#ff0000', strokeWidth: 3, hitStrokeWidth: 50, opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant }, { name: 'ancr_flec' + id + '_1', objet: 'ancre', x: this.positionX1, y: this.positionY1, radius: 10 / this.echelle, width: 10 / this.echelle, height: 10 / this.echelle, fill: '#ffff00', stroke: '#000000', strokeWidth: 1, visible: true, opacity: 1, draggable: true }, { name: 'ancr_flec' + id + '_2', objet: 'ancre', x: this.positionX2, y: this.positionY2, radius: 10 / this.echelle, width: 10 / this.echelle, height: 10 / this.echelle, fill: '#ffff00', stroke: '#000000', strokeWidth: 1, visible: true, opacity: 1, draggable: true }]
				nom = 'flec' + id
				objet = 'fleche'
				break
			case 'texte':
				items = [{ name: 'text' + id, objet: 'texte', text: this.texte, fontSize: 25, lineHeight: 1.25, verticalAlign: 'middle', padding: 10, x: this.positionX1, y: this.positionY1, rotation: 0, fill: '#000000', opacity: 1, wrap: 'word', draggable: true, verrouille: false, identifiant: this.identifiant }]
				nom = 'text' + id
				objet = 'texte'
				break
			case 'label':
				items = [{ name: 'labl' + id, objet: 'label', x: this.positionX1, y: this.positionY1, rotation: 0, opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant, tag: { name: 'labl' + id, objet: 'label', shadowColor: '#222222', shadowBlur: 10, shadowOffset: [7, 7], shadowOpacity: 0.2, fill: '#ffff00' }, text: { name: 'labl' + id, objet: 'label', text: this.texte, fontSize: 25, lineHeight: 1.25, verticalAlign: 'middle', fill: '#000000', padding: 20, wrap: 'word', align: 'center' } }]
				nom = 'labl' + id
				objet = 'label'
				break
			case 'image':
				items = [{ name: 'imag' + id, objet: 'image', image: this.image.html, fichier: this.image.fichier, width: largeur, height: hauteur, x: (this.largeur / 2) - (largeur / 2), y: (this.hauteur / 2) - (hauteur / 2), rotation: 0, filtre: 'Aucun', opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant }]
				nom = 'imag' + id
				objet = 'image'
				this.image = { html: '', fichier: '' }
				break
			case 'commentaire':
				items = [{ name: 'comm' + id, objet: 'commentaire', x: this.positionX1, y: this.positionY1, opacity: 1, draggable: true, verrouille: false, identifiant: this.identifiant, tag: { name: 'comm' + id, objet: 'commentaire', fill: '#dddddd', cornerRadius: 5, stroke: '#000000', strokeWidth: 1 }, text: { name: 'comm' + id, objet: 'commentaire', text: (total + 1), fontSize: 18, lineHeight: 1, verticalAlign: 'middle', fill: '#000000', padding: 10, align: 'center' } }]
				nom = 'comm' + id
				objet = 'commentaire'
				break
			}
			this.items[this.page].push(...items)
			this.nom = nom
			this.objet = objet
			if (type !== 'commentaire') {
				this.enregistrer('ajouter', '', items)
			} else {
				this.itemId = nom
				let nomUtilisateur = ''
				this.utilisateurs.forEach(function (utilisateur) {
					if (utilisateur.identifiant === this.identifiant) {
						nomUtilisateur = utilisateur.nom
					}
				}.bind(this))
				const commentaireId = (new Date()).getTime() + Math.random().toString(16).slice(10)
				const commentaire = { id: commentaireId, texte: this.commentaire, identifiant: this.identifiant, nom: nomUtilisateur }
				this.$socket.emit('enregistrercommentaire', { tableau: this.tableau, itemId: this.itemId, commentaire: commentaire, page: this.page })
			}
			this.$nextTick(function () {
				this.transformer()
				const copieItems = JSON.parse(JSON.stringify(items))
				copieItems.forEach(function (item) {
					if (item.objet === 'image') {
						delete item.image
					}
				})
				if (this.objet === 'image') { // fix pour sélection du filtre
					this.nom = ''
					this.objet = ''
					this.transformer()
					this.$nextTick(function () {
						this.nom = nom
						this.objet = objet
						this.transformer()
					}.bind(this))
				}
				this.$socket.emit('ajouter', { tableau: this.tableau, page: this.page, items: copieItems })
			}.bind(this))
		},
		dessinerForme (type) {
			this.ajouter(type)
			this.reinitialiserOutils()
			this.creation = false
		},
		verrouiller () {
			let item
			const noms = []
			if (this.objet !== 'selection') {
				item = this.items[this.page].find(r => r.name === this.nom)
				if (item) {
					item.draggable = false
					item.verrouille = true
					noms.push(this.nom)
				}
			} else {
				const objets = this.$refs.transformer.getNode().nodes()
				for (let i = 0; i < objets.length; i++) {
					item = this.items[this.page].find(r => r.name === objets[i].getAttrs().name)
					if (item) {
						item.draggable = false
						item.verrouille = true
						noms.push(objets[i].getAttrs().name)
					}
				}
			}
			this.objetVerrouille = true
			this.items[this.page].forEach(function (item, index) {
				if (item.objet === 'ancre') {
					this.items[this.page][index].visible = false
				}
			}.bind(this))
			if (this.objet !== 'selection') {
				this.transformer()
			} else {
				this.$refs.transformer.getNode().getLayer().batchDraw()
			}
			this.enregistrer('verrouiller', noms, '')
			this.$socket.emit('verrouiller', { tableau: this.tableau, page: this.page, noms: noms })
		},
		deverrouiller () {
			let item
			const noms = []
			if (this.objet !== 'selection') {
				item = this.items[this.page].find(r => r.name === this.nom)
				if (item && item.objet !== 'dessin') {
					item.draggable = true
					item.verrouille = false
					noms.push(this.nom)
				}
			} else {
				const objets = this.$refs.transformer.getNode().nodes()
				for (let i = 0; i < objets.length; i++) {
					item = this.items[this.page].find(r => r.name === objets[i].getAttrs().name)
					if (item && item.objet !== 'dessin') {
						item.draggable = true
						item.verrouille = false
						noms.push(objets[i].getAttrs().name)
					}
				}
			}
			this.objetVerrouille = false
			const type = item.name.substring(0, 4)
			if (type === 'flec' || type === 'line') {
				const ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + item.name + '_1')
				const ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + item.name + '_2')
				ancre1.visible = true
				ancre2.visible = true
			}
			if (this.objet !== 'selection') {
				this.nom = item.name
				this.objet = item.objet
				this.transformer()
			} else {
				this.reinitialiserSelection()
				this.$refs.transformer.getNode().getLayer().batchDraw()
			}
			this.enregistrer('deverrouiller', noms, '')
			this.$socket.emit('deverrouiller', { tableau: this.tableau, page: this.page, noms: noms })
		},
		mettreDevant () {
			let indexItem
			this.items[this.page].forEach(function (item, index) {
				if (item.name === this.nom && index < this.items[this.page].length) {
					this.items[this.page].splice(index, 1)
					this.items[this.page].splice(index + 1, 0, item)
					indexItem = index + 1
				}
			}.bind(this))
			this.$nextTick(function () {
				const objets = this.$refs.transformer.getNode().getStage().find((item) => {
					return item.getAttrs().objet && item.getAttrs().objet === 'image'
				})
				for (let i = 0; i < objets.length; i++) {
					const filtre = objets[i].getAttrs().filtre
					if (filtre === 'Aucun') {
						objets[i].filters([])
					} else {
						objets[i].cache()
						objets[i].filters([Konva.Filters[filtre]])
						if (filtre === 'Pixelate') {
							objets[i].pixelSize(10)
						}
					}
				}
				this.transformer()
			}.bind(this))
			if (this.chrono !== '') {
				clearTimeout(this.chrono)
				this.chrono = ''
			}
			this.chrono = setTimeout(function () {
				this.enregistrer('modifierposition', this.nom, indexItem)
				this.$socket.emit('modifierposition', { tableau: this.tableau, page: this.page, nom: this.nom, index: indexItem })
				this.chrono = ''
			}.bind(this), 400)
		},
		mettreDerriere () {
			let indexItem
			this.items[this.page].forEach(function (item, index) {
				if (item.name === this.nom && index > 0) {
					this.items[this.page].splice(index, 1)
					this.items[this.page].splice(index - 1, 0, item)
					indexItem = index - 1
				}
			}.bind(this))
			this.$nextTick(function () {
				const objets = this.$refs.transformer.getNode().getStage().find((item) => {
					return item.getAttrs().objet && item.getAttrs().objet === 'image'
				})
				for (let i = 0; i < objets.length; i++) {
					const filtre = objets[i].getAttrs().filtre
					if (filtre === 'Aucun') {
						objets[i].filters([])
					} else {
						objets[i].cache()
						objets[i].filters([Konva.Filters[filtre]])
						if (filtre === 'Pixelate') {
							objets[i].pixelSize(10)
						}
					}
				}
				this.transformer()
			}.bind(this))
			if (this.chrono !== '') {
				clearTimeout(this.chrono)
				this.chrono = ''
			}
			this.chrono = setTimeout(function () {
				this.enregistrer('modifierposition', this.nom, indexItem)
				this.$socket.emit('modifierposition', { tableau: this.tableau, page: this.page, nom: this.nom, index: indexItem })
				this.chrono = ''
			}.bind(this), 400)
		},
		reduireTransparence () {
			const item = this.items[this.page].find(r => r.name === this.nom)
			const transparence = parseFloat(item.opacity.toFixed(1))
			if (transparence > 0.1) {
				item.opacity = transparence - 0.1
			}
			if (this.chrono !== '') {
				clearTimeout(this.chrono)
				this.chrono = ''
			}
			this.chrono = setTimeout(function () {
				this.enregistrer('modifiertransparence', this.nom, item.opacity)
				this.$socket.emit('modifiertransparence', { tableau: this.tableau, page: this.page, nom: this.nom, transparence: item.opacity })
				this.chrono = ''
			}.bind(this), 400)
		},
		augmenterTransparence () {
			const item = this.items[this.page].find(r => r.name === this.nom)
			const transparence = parseFloat(item.opacity.toFixed(1))
			if (transparence < 1) {
				item.opacity = transparence + 0.1
			}
			if (this.chrono !== '') {
				clearTimeout(this.chrono)
				this.chrono = ''
			}
			this.chrono = setTimeout(function () {
				this.enregistrer('modifiertransparence', this.nom, item.opacity)
				this.$socket.emit('modifiertransparence', { tableau: this.tableau, page: this.page, nom: this.nom, transparence: item.opacity })
				this.chrono = ''
			}.bind(this), 400)
		},
		ajouterTexte () {
			this.ajouter(this.outil)
			this.reinitialiserOutils()
			this.fermerModaleTexte()
			this.selection = false
			this.activerSelecteur()
		},
		modifierTexte (event) {
			const texte = event.target.text()
			this.texte = texte
			this.modale = 'texte'
			setTimeout(function () {
				document.querySelector('#texte textarea').focus()
			}, 10)
		},
		enregistrerTexte () {
			const type = this.nom.substring(0, 4)
			const item = this.items[this.page].find(r => r.name === this.nom)
			const texte = this.texte
			if (type === 'text') {
				item.text = texte
			} else {
				item.text.text = texte
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}
			this.fermerModaleTexte()
			this.enregistrer('modifiertexte', this.nom, texte)
			this.$socket.emit('modifiertexte', { tableau: this.tableau, page: this.page, nom: this.nom, texte: texte })
		},
		fermerModaleTexte () {
			this.texte = ''
			this.modale = ''
			this.gererFocus()
		},
		reduireTailleTexte () {
			const item = this.items[this.page].find(r => r.name === this.nom)
			const taille = item.fontSize
			item.fontSize = taille - 2
			if (this.chrono !== '') {
				clearTimeout(this.chrono)
				this.chrono = ''
			}
			this.chrono = setTimeout(function () {
				this.enregistrer('modifiertailletexte', this.nom, taille - 2)
				this.$socket.emit('modifiertailletexte', { tableau: this.tableau, page: this.page, nom: this.nom, taille: taille - 2 })
				this.chrono = ''
			}.bind(this), 400)
		},
		augmenterTailleTexte () {
			const item = this.items[this.page].find(r => r.name === this.nom)
			const taille = item.fontSize
			item.fontSize = taille + 2
			if (this.chrono !== '') {
				clearTimeout(this.chrono)
				this.chrono = ''
			}
			this.chrono = setTimeout(function () {
				this.enregistrer('modifiertailletexte', this.nom, taille + 2)
				this.$socket.emit('modifiertailletexte', { tableau: this.tableau, page: this.page, nom: this.nom, taille: taille + 2 })
				this.chrono = ''
			}.bind(this), 400)
		},
		alignerTexte (alignement) {
			const item = this.items[this.page].find(r => r.name === this.nom)
			item.align = alignement
			item.opacity = 0.9
			item.opacity = 1 // pour activer modif alignement...
			this.enregistrer('modifieralignementtexte', this.nom, alignement)
			this.$socket.emit('modifieralignementtexte', { tableau: this.tableau, page: this.page, nom: this.nom, alignement: alignement })
		},
		formaterTexte (style) {
			let item = this.items[this.page].find(r => r.name === this.nom)
			if (item.objet === 'label') {
				item = item.text
			}
			if (style === 'bold' || style === 'italic') {
				const fontStyle = item.fontStyle
				if (fontStyle && fontStyle !== '' && ((fontStyle === 'bold' && style === 'italic') || (fontStyle === 'italic' && style === 'bold'))) {
					item.fontStyle = 'italic bold'
				} else if (fontStyle && (fontStyle === 'bold' || fontStyle === 'italic')) {
					item.fontStyle = 'normal'
				} else if (fontStyle && fontStyle === 'italic bold' && style === 'bold') {
					item.fontStyle = 'italic'
				} else if (fontStyle && fontStyle === 'italic bold' && style === 'italic') {
					item.fontStyle = 'bold'
				} else if (!fontStyle || fontStyle === 'normal') {
					item.fontStyle = style
				}
				this.enregistrer('modifierstyletexte', this.nom, item.fontStyle)
				this.$socket.emit('modifierstyletexte', { tableau: this.tableau, page: this.page, nom: this.nom, style: item.fontStyle })
			} else if (style === 'underline' || style === 'line-through') {
				const textDecoration = item.textDecoration
				if (textDecoration && textDecoration !== '' && ((textDecoration === 'underline' && style === 'line-through') || (textDecoration === 'line-through' && style === 'underline'))) {
					item.textDecoration = 'underline line-through'
				} else if (textDecoration && (textDecoration === 'underline' || textDecoration === 'line-through')) {
					item.textDecoration = 'normal'
				} else if (textDecoration && textDecoration === 'underline line-through' && style === 'underline') {
					item.textDecoration = 'line-through'
				} else if (textDecoration && textDecoration === 'underline line-through' && style === 'line-through') {
					item.textDecoration = 'underline'
				} else if (!textDecoration || textDecoration === 'normal') {
					item.textDecoration = style
				}
				this.enregistrer('modifierdecorationtexte', this.nom, item.textDecoration)
				this.$socket.emit('modifierdecorationtexte', { tableau: this.tableau, page: this.page, nom: this.nom, decoration: item.textDecoration })
			}
		},
		televerserImage (image, type) {
			const formats = ['jpg', 'jpeg', 'png', 'gif', 'svg']
			const extension = image.name.substring(image.name.lastIndexOf('.') + 1).toLowerCase()
			if (image && formats.includes(extension) && image.size < this.limite * 1024000) {
				this.chargement = true
				const formulaire = new FormData()
				formulaire.append('tableau', this.tableau)
				formulaire.append('fichier', image)
				axios.post(this.hote + '/api/televerser-image', formulaire, {
					headers: {
						'Content-Type': 'multipart/form-data'
					}
				}).then(function (reponse) {
					const donnees = reponse.data
					if (donnees === 'erreur_televersement') {
						this.chargement = false
						this.message = this.$t('erreurTeleversementImage')
					} else if (donnees === 'erreur_type_fichier') {
						this.chargement = false
						this.message = this.$t('formatFichierPasAccepte')
					} else if (donnees !== '') {
						const image = new window.Image()
						image.src = '/fichiers/' + this.tableau + '/' + donnees
						image.onload = function () {
							this.image.html = image
							this.image.fichier = donnees
							this.ajouter('image')
							this.chargement = false
						}.bind(this)
						image.onerror = function () {
							this.chargement = false
						}.bind(this)
					}
					if (type === 'input') {
						document.querySelector('#televerser-image').value = ''
					}
					this.reinitialiserOutils()
				}.bind(this)).catch(function () {
					this.chargement = false
					if (type === 'input') {
						document.querySelector('#televerser-image').value = ''
					}
					this.message = this.$t('erreurCommunicationServeur')
					this.reinitialiserOutils()
				}.bind(this))
			} else {
				if (formats.includes(extension) === false) {
					this.message = this.$t('formatFichierPasAccepte')
				} else if (champ.files[0].size > this.limite * 1024000) {
					this.message = this.$t('tailleMaximale', { taille: this.limite })
				}
				if (type === 'input') {
					document.querySelector('#televerser-image').value = ''
				}
				this.reinitialiserOutils()
			}
		},
		modifierFiltre (filtre) {
			const objets = this.$refs.transformer.getNode().nodes()
			for (let i = 0; i < objets.length; i++) {
				if (this.nom === objets[i].getAttrs().name) {
					if (filtre === 'Aucun') {
						objets[i].filters([])
					} else {
						objets[i].cache()
						objets[i].filters([Konva.Filters[filtre]])
						if (filtre === 'Pixelate') {
							objets[i].pixelSize(10)
						}
					}
					this.$nextTick(function () {
						this.transformer()
						this.items[this.page].forEach(function (item) {
							if (item.name === this.nom) {
								item.filtre = filtre
							}
						}.bind(this))
						this.enregistrer('modifierfiltre', this.nom, filtre)
						this.$socket.emit('modifierfiltre', { tableau: this.tableau, page: this.page, nom: this.nom, filtre: filtre })
					}.bind(this))
				}
			}
		},
		definirFiltre () {
			const objets = this.$refs.transformer.getNode().nodes()
			let filtre
			for (let i = 0; i < objets.length; i++) {
				if (this.nom === objets[i].getAttrs().name && objets[i].getFilters() && objets[i].getFilters()[0]) {
					filtre = objets[i].getFilters()[0].name
				} else {
					filtre = 'Aucun'
				}
			}
			return filtre
		},
		rechercher (page) {
			if (this.requete !== '') {
				const requete = this.requete.replace(/\s+/g, '+')
				const xhr = new XMLHttpRequest()
				xhr.onload = function () {
					if (xhr.readyState === xhr.DONE && xhr.status === 200) {
						const donnees = JSON.parse(xhr.responseText)
						if (donnees && donnees.total > 0) {
							this.resultats = donnees
							this.$nextTick(function () {
								// eslint-disable-next-line
								new flexImages({ selector: '.resultats', rowHeight: 65 })
							})
						}
					}
				}.bind(this)
				if (page === 1) {
					this.pageRecherche = page
				}
				xhr.open('GET', 'https://pixabay.com/api/?key=' + this.pixabayAPI + '&q=' + requete + '&image_type=photo&lang=fr&orientation=horizontal&safesearch=true&per_page=16&page=' + page, true)
				xhr.send()
			}
		},
		modifierPageRecherche (type) {
			if (type === 'suivante' && this.pageRecherche < (this.resultats.total / 16)) {
				this.pageRecherche++
			} else if (type === 'precedente' && this.pageRecherche > 1) {
				this.pageRecherche--
			}
		},
		ajouterImage (src) {
			this.chargement = true
			const image = new window.Image()
			image.crossOrigin = 'Anonymous'
			image.src = src
			image.onload = function () {
				this.image.html = image
				this.image.fichier = src
				this.ajouter('image')
				this.chargement = false
				this.modale = ''
			}.bind(this)
			image.onerror = function () {
				this.chargement = false
			}.bind(this)
			this.reinitialiserOutils()
		},
		fermerModaleRecherche () {
			this.modale = ''
			this.resultats = {}
			this.requete = ''
			this.pageRecherche = 1
			this.gererFocus()
		},
		ajouterCommentaire () {
			if (this.commentaire !== '') {
				this.ajouter(this.outil)
				this.reinitialiserOutils()
				this.selection = false
				this.activerSelecteur()
			}
		},
		gererCommentaires () {
			this.itemId = this.nom
			if (this.commentaires.hasOwnProperty(this.nom) === true) {
				this.commentairesItem = JSON.parse(JSON.stringify(this.commentaires[this.nom]))
			}
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'commentaire'
			this.$nextTick(function () {
				this.genererEditeur()
				document.querySelector('#editeur .contenu-editeur-commentaire').focus()
			}.bind(this))
		},
		afficherModifierCommentaire (id, texte) {
			this.commentaireId = id
			this.commentaireModifie = texte
			this.editionCommentaire = true
			this.emojis = ''
			this.$nextTick(function () {
				const actions = this.definirActionsEditeur('commentaire-modifie')
				const editeur = pell.init({
					element: document.querySelector('#commentaire-modifie'),
					onChange: function (html) {
						let commentaire = html.replace(/(<a [^>]*)(target="[^"]*")([^>]*>)/gi, '$1$3')
						commentaire = commentaire.replace(/(<a [^>]*)(>)/gi, '$1 target="_blank"$2')
						commentaire = linkifyHtml(commentaire, {
							defaultProtocol: 'https',
							target: '_blank'
						})
						this.commentaireModifie = commentaire
					}.bind(this),
					actions: actions,
					classes: { actionbar: 'boutons-editeur-commentaire', button: 'bouton-editeur', content: 'contenu-editeur-commentaire', selected: 'bouton-actif' }
				})
				editeur.content.innerHTML = this.commentaireModifie
				editeur.onpaste = function (event) {
					event.preventDefault()
					event.stopPropagation()
					let html = event.clipboardData.getData('text/html')
					if (html !== '') {
						html = stripTags(html, ['b', 'i', 'u', 'a', 'br', 'div', 'font', 'ul', 'ol'])
						html = html.replace(/style=".*?"/mg, '')
						html = html.replace(/class=".*?"/mg, '')
						pell.exec('insertHTML', html)
					} else {
						pell.exec('insertText', event.clipboardData.getData('text/plain'))
					}
				}
				document.querySelector('#couleur-texte-commentaire-modifie').addEventListener('change', this.modifierCouleurCommentaire)
			}.bind(this))
		},
		annulerModifierCommentaire () {
			this.commentaireId = ''
			this.commentaireModifie = ''
			this.editionCommentaire = false
			this.emojis = ''
		},
		enregistrerCommentaire () {
			if (this.commentaire !== '') {
				const id = (new Date()).getTime() + Math.random().toString(16).slice(10)
				let nomUtilisateur = ''
				this.utilisateurs.forEach(function (utilisateur) {
					if (utilisateur.identifiant === this.identifiant) {
						nomUtilisateur = utilisateur.nom
					}
				}.bind(this))
				const commentaire = { id: id, texte: this.commentaire, identifiant: this.identifiant, nom: nomUtilisateur }
				this.$socket.emit('enregistrercommentaire', { tableau: this.tableau, itemId: this.itemId, commentaire: commentaire, page: this.page })
			}
		},
		enregistrerModifierCommentaire () {
			if (this.commentaireModifie !== '') {
				const commentaire = { id: this.commentaireId, texte: this.commentaireModifie, identifiant: this.identifiant }
				this.$socket.emit('enregistrercommentairemodifie', { tableau: this.tableau, itemId: this.itemId, commentaire: commentaire })
			}
		},
		supprimerCommentaire (commentaireId) {
			this.editionCommentaire = false
			this.$socket.emit('supprimercommentaire', { tableau: this.tableau, itemId: this.itemId, commentaireId: commentaireId })
		},
		fermerModaleCommentaire () {
			this.modale = ''
			this.itemId = ''
			this.editeur = ''
			this.emojis = ''
			this.commentaire = ''
			this.commentairesItem = []
			this.commentaireId = ''
			this.commentaireModifie = ''
			this.editionCommentaire = false
			this.gererFocus()
		},
		genererEditeur () {
			if (this.editeur === '') {
				const actions = this.definirActionsEditeur('commentaire')
				this.editeur = pell.init({
					element: document.querySelector('#editeur'),
					onChange: function (html) {
						let commentaire = html.replace(/(<a [^>]*)(target="[^"]*")([^>]*>)/gi, '$1$3')
						commentaire = commentaire.replace(/(<a [^>]*)(>)/gi, '$1 target="_blank"$2')
						commentaire = linkifyHtml(commentaire, {
							defaultProtocol: 'https',
							target: '_blank'
						})
						this.commentaire = commentaire
					}.bind(this),
					actions: actions,
					classes: { actionbar: 'boutons-editeur-commentaire', button: 'bouton-editeur', content: 'contenu-editeur-commentaire', selected: 'bouton-actif' }
				})
				this.editeur.onpaste = function (event) {
					event.preventDefault()
					event.stopPropagation()
					let html = event.clipboardData.getData('text/html')
					if (html !== '') {
						html = stripTags(html, ['b', 'i', 'u', 'a', 'br', 'div', 'font', 'ul', 'ol'])
						html = html.replace(/style=".*?"/mg, '')
						html = html.replace(/class=".*?"/mg, '')
						pell.exec('insertHTML', html)
					} else {
						pell.exec('insertText', event.clipboardData.getData('text/plain'))
					}
				}
				document.querySelector('#couleur-texte-commentaire').addEventListener('change', this.modifierCouleurCommentaire)
			}
		},
		definirActionsEditeur (type) {
			let actions = [
				{ name: 'gras', title: this.$t('gras'), icon: '<i class="material-icons">format_bold</i>', result: () => pell.exec('bold') },
				{ name: 'italique', title: this.$t('italique'), icon: '<i class="material-icons">format_italic</i>', result: () => pell.exec('italic') },
				{ name: 'souligne', title: this.$t('souligne'), icon: '<i class="material-icons">format_underlined</i>', result: () => pell.exec('underline') },
				{ name: 'barre', title: this.$t('barre'), icon: '<i class="material-icons">format_strikethrough</i>', result: () => pell.exec('strikethrough') },
				{ name: 'couleur', title: this.$t('couleurTexte'), icon: '<label for="couleur-texte-' + type + '"><i class="material-icons">format_color_text</i></label><input id="couleur-texte-' + type + '" type="color">', result: () => undefined },
				{ name: 'lien', title: this.$t('lien'), icon: '<i class="material-icons">link</i>', result: () => { const url = window.prompt(this.$t('adresseLien')); if (url) { pell.exec('createLink', url) } } },
				{ name: 'emojis', title: this.$t('emoticones'), icon: '<i class="material-icons">insert_emoticon</i>', result: function () { this.afficherEmojis(type) }.bind(this) }
			]
			if (((this.userAgent.match(/iPhone/i) || this.userAgent.match(/iPad/i) || this.userAgent.match(/iPod/i)) && this.userAgent.match(/Mobile/i)) || this.userAgent.match(/Android/i)) {
				actions = [{ name: 'gras', title: this.$t('gras'), icon: '<i class="material-icons">format_bold</i>', result: () => pell.exec('bold') }, { name: 'italique', title: this.$t('italique'), icon: '<i class="material-icons">format_italic</i>', result: () => pell.exec('italic') }, { name: 'souligne', title: this.$t('souligne'), icon: '<i class="material-icons">format_underlined</i>', result: () => pell.exec('underline') }, { name: 'barre', title: this.$t('barre'), icon: '<i class="material-icons">format_strikethrough</i>', result: () => pell.exec('strikethrough') }]
			}
			return actions
		},
		afficherEmojis (type) {
			if (type === 'commentaire' && this.emojis !== 'commentaire') {
				this.emojis = 'commentaire'
			} else if (type === 'commentaire' && this.emojis === 'commentaire') {
				this.emojis = ''
			} else if (type === 'commentaire-modifie' && this.emojis !== 'commentaire-modifie') {
				this.emojis = 'commentaire-modifie'
			} else if (type === 'commentaire-modifie' && this.emojis === 'commentaire-modifie') {
				this.emojis = ''
			}
		},
		insererEmoji (emoji) {
			pell.exec('insertText', emoji)
		},
		modifierCouleurCommentaire (event) {
			pell.exec('foreColor', event.target.value)
		},
		formaterDateRelative (date, langue) {
			return dayjs(new Date(date)).locale(langue).fromNow()
		},
		afficherItemCommentaire (itemId) {
			this.selectionner(itemId)
			this.transformer()
		},
		gererAccordeon (itemId) {
			if (this.accordeonOuvert === itemId) {
				this.accordeonOuvert = ''
			} else {
				this.accordeonOuvert = itemId
			}
		},
		definirNumItem (itemId) {
			let num
			this.items[this.page].forEach(function (item) {
				if (item.name === itemId) {
					num = item.text.text
				}
			})
			return num
		},
		definirNomUtilisateur (donnees) {
			let nom = ''
			this.utilisateurs.forEach(function (utilisateur) {
				if (utilisateur.identifiant === donnees.identifiant) {
					nom = utilisateur.nom
				}
			})
			if (nom === '' && donnees.hasOwnProperty('nom') === true && donnees.nom !== '') {
				nom = donnees.nom
			} else if (nom === ''&& (donnees.hasOwnProperty('nom') === false || (donnees.hasOwnProperty('nom') === true && donnees.nom === ''))) {
				nom = donnees.identifiant.substring(0, 6).toUpperCase()
			}
			return nom
		},
		definirCouleurSelecteur (objet, item) {
			switch (objet) {
			case 'rectangle':
			case 'cercle':
			case 'ligne':
			case 'fleche':
			case 'dessin':
				if (['#000000', '#ffffff', '#ff0000', '#ffff00', '#00ff00', '#04fdff', '#cccccc'].includes(item.stroke) === false) {
					this.couleurSelecteur = item.stroke
				}
				break
			case 'rectangle-plein':
			case 'cercle-plein':
			case 'etoile':
			case 'surlignage':
			case 'texte':
				if (['#000000', '#ffffff', '#ff0000', '#ffff00', '#00ff00', '#04fdff', '#cccccc'].includes(item.fill) === false) {
					this.couleurSelecteur = item.fill
				}
				break
			case 'label':
			case 'commentaire':
				if (['#000000', '#ffffff', '#ff0000', '#ffff00', '#00ff00', '#04fdff', '#cccccc'].includes(item.tag.fill) === false) {
					this.couleurSelecteur = item.tag.fill
				}
				break
			}
		},
		definirCouleur () {
			const couleur = document.querySelector('#couleur-selecteur').value
			if (this.outilDessiner && this.couleurSelecteur !== '#000000') {
				this.couleurStylo = couleur
			} else if (!this.outilDessiner && this.couleurSelecteur !== '#000000') {
				this.modifierCouleur(couleur)
			}
		},
		modifierCouleurSelecteur (event) {
			this.couleurSelecteur = event.target.value
			this.modifierCouleur(event.target.value)
		},
		modifierCouleur (couleur) {
			if (this.outilSelectionner) {
				let item = this.items[this.page].find(r => r.name === this.nom)
				if (this.objet === 'ancre') {
					item = this.items[this.page].find(r => r.name === this.nom.split('_')[1])
				}
				switch (this.objet) {
				case 'rectangle':
				case 'cercle':
				case 'dessin':
					item.stroke = couleur
					break
				case 'rectangle-plein':
				case 'cercle-plein':
				case 'etoile':
				case 'surlignage':
				case 'texte':
					item.fill = couleur
					break
				case 'ligne':
				case 'fleche':
				case 'ancre':
					item.stroke = couleur
					item.fill = couleur
					break
				case 'label':
				case 'commentaire':
					item.tag.fill = couleur
					break
				}
				this.enregistrer('modifiercouleur', this.nom, couleur)
				this.$socket.emit('modifiercouleur', { tableau: this.tableau, page: this.page, nom: this.nom, couleur: couleur })
			} else if (this.outilDessiner) {
				this.couleurStylo = couleur
			}
		},
		modifierEpaisseur (epaisseur) {
			this.epaisseurStylo = epaisseur
		},
		modifierTransparence (transparence) {
			this.transparenceStylo = transparence
		},
		copier () {
			if (this.nom !== '' && this.objet !== 'selection') {
				let itemCopie = {}
				JSON.parse(JSON.stringify(this.items[this.page])).forEach(function (item) {
					if (item.name.includes(this.nom)) {
						itemCopie = item
					}
				}.bind(this))
				this.itemCopie = itemCopie
			}
		},
		coller (event) {
			if (Object.keys(this.itemCopie).length > 0) {
				const id = (new Date()).getTime() + Math.random().toString(16).slice(10)
				const items = []
				let nom, objet
				items.push(this.itemCopie)
				if (this.itemCopie.name.substring(0, 4) === 'imag') {
					this.chargement = true
					const fichier = this.itemCopie.fichier
					const filtre = this.itemCopie.filtre
					if (this.verifierURL(fichier) === false) {
						axios.post(this.hote + '/api/copier-image', {
							tableau: this.tableau,
							image: fichier
						}).then(function (reponse) {
							const donnees = reponse.data
							if (donnees === 'erreur') {
								this.chargement = false
								this.message = this.$t('erreurCommunicationServeur')
							} else if (donnees !== '') {
								const image = new window.Image()
								image.src = '/fichiers/' + this.tableau + '/' + fichier
								image.onload = function () {
									this.chargement = false
									items.forEach(function (item) {
										const type = item.name.substring(0, 4)
										nom = type + id
										objet = item.objet
										item.name = nom
										item.image = image
										item.fichier = fichier
									})
									this.items[this.page].forEach(function (item, index) {
										if (item.objet === 'ancre') {
											this.items[this.page][index].visible = false
										}
									}.bind(this))
									this.$nextTick(function () {
										this.items[this.page].push(...items)
										this.nom = nom
										this.objet = objet
										this.enregistrer('ajouter', '', items)
										this.$nextTick(function () {
											const objets = this.$refs.transformer.getNode().getStage().find((item) => {
												return item.getAttrs().objet && item.getAttrs().objet === 'image'
											})
											for (let i = 0; i < objets.length; i++) {
												if (nom === objets[i].getAttrs().name) {
													if (filtre === 'Aucun') {
														objets[i].filters([])
													} else {
														objets[i].cache()
														objets[i].filters([Konva.Filters[filtre]])
														if (filtre === 'Pixelate') {
															objets[i].pixelSize(10)
														}
													}
												}
											}
											this.transformer()
											const copieItems = JSON.parse(JSON.stringify(items))
											copieItems.forEach(function (item) {
												if (item.objet === 'image') {
													delete item.image
												}
											})
											this.nom = ''
											this.objet = ''
											this.itemCopie = {}
											this.transformer()
											this.$nextTick(function () {
												this.nom = nom
												this.objet = objet
												this.transformer()
											}.bind(this))
											this.$socket.emit('ajouter', { tableau: this.tableau, page: this.page, items: copieItems })
										}.bind(this))
									}.bind(this))
								}.bind(this)
								image.onerror = function () {
									this.chargement = false
								}.bind(this)
							}
						}.bind(this)).catch(function () {
							this.message = this.$t('erreurCommunicationServeur')
						}.bind(this))
					} else {
						const image = new window.Image()
						image.crossOrigin = 'Anonymous'
						image.src = fichier
						image.onload = function () {
							this.chargement = false
							items.forEach(function (item) {
								const type = item.name.substring(0, 4)
								nom = type + id
								objet = item.objet
								item.name = nom
								item.image = image
								item.fichier = fichier
							})
							this.items[this.page].forEach(function (item, index) {
								if (item.objet === 'ancre') {
									this.items[this.page][index].visible = false
								}
							}.bind(this))
							this.$nextTick(function () {
								this.items[this.page].push(...items)
								this.nom = nom
								this.objet = objet
								this.enregistrer('ajouter', '', items)
								this.$nextTick(function () {
									const objets = this.$refs.transformer.getNode().getStage().find((item) => {
										return item.getAttrs().objet && item.getAttrs().objet === 'image'
									})
									for (let i = 0; i < objets.length; i++) {
										if (nom === objets[i].getAttrs().name) {
											if (filtre === 'Aucun') {
												objets[i].filters([])
											} else {
												objets[i].cache()
												objets[i].filters([Konva.Filters[filtre]])
												if (filtre === 'Pixelate') {
													objets[i].pixelSize(10)
												}
											}
										}
									}
									this.transformer()
									const copieItems = JSON.parse(JSON.stringify(items))
									copieItems.forEach(function (item) {
										if (item.objet === 'image') {
											delete item.image
										}
									})
									this.nom = ''
									this.objet = ''
									this.itemCopie = {}
									this.transformer()
									this.$nextTick(function () {
										this.nom = nom
										this.objet = objet
										this.transformer()
									}.bind(this))
									this.$socket.emit('ajouter', { tableau: this.tableau, page: this.page, items: copieItems })
								}.bind(this))
							}.bind(this))
						}.bind(this)
						image.onerror = function () {
							this.chargement = false
						}.bind(this)
					}
				} else {
					items.forEach(function (item) {
						const type = item.name.substring(0, 4)
						if (type === 'ancr') {
							item.name = item.name.substring(0, 9) + id + item.name.substring(item.name.length - 2)
						} else {
							nom = type + id
							objet = item.objet
							item.name = nom
						}
						if (type === 'labl') {
							item.tag.name = type + id
							item.text.name = type + id
						}
					})
					this.items[this.page].forEach(function (item, index) {
						if (item.objet === 'ancre') {
							this.items[this.page][index].visible = false
						}
					}.bind(this))
					this.$nextTick(function () {
						this.items[this.page].push(...items)
						this.nom = nom
						this.objet = objet
						this.itemCopie = {}
						this.enregistrer('ajouter', '', items)
						this.$nextTick(function () {
							this.transformer()
							const copieItems = JSON.parse(JSON.stringify(items))
							this.itemsDupliques.push(...copieItems)
							if (this.chrono !== '') {
								clearTimeout(this.chrono)
								this.chrono = ''
							}
							this.chrono = setTimeout(function () {
								this.$socket.emit('ajouter', { tableau: this.tableau, page: this.page, items: this.itemsDupliques })
								this.chrono = ''
								this.itemsDupliques = []
							}.bind(this), 400)
						}.bind(this))
					}.bind(this))
				}
			} else {
				const items = event.clipboardData.files
				for (let i = 0; i < items.length; i++) {
					if (items[i].type.includes('image')) {
						this.televerserImage(items[i], 'paste')
					}
				}
			}
		},
		dupliquer () {
			if (this.nom !== '' && this.objet !== 'selection') {
				const id = (new Date()).getTime() + Math.random().toString(16).slice(10)
				const items = []
				const itemsPage = JSON.parse(JSON.stringify(this.items[this.page]))
				let nom, objet
				if (this.nom.substring(0, 4) === 'imag') {
					this.chargement = true
					let fichier = ''
					let filtre = ''
					itemsPage.forEach(function (item) {
						if (item.name.includes(this.nom)) {
							fichier = item.fichier
							filtre = item.filtre
							items.push(item)
						}
					}.bind(this))
					if (fichier !== '' && filtre !== '' && this.verifierURL(fichier) === false) {
						axios.post(this.hote + '/api/copier-image', {
							tableau: this.tableau,
							image: fichier
						}).then(function (reponse) {
							const donnees = reponse.data
							if (donnees === 'erreur') {
								this.chargement = false
								this.message = this.$t('erreurCommunicationServeur')
							} else if (donnees !== '') {
								const image = new window.Image()
								image.src = '/fichiers/' + this.tableau + '/' + fichier
								image.onload = function () {
									this.chargement = false
									items.forEach(function (item) {
										const type = item.name.substring(0, 4)
										nom = type + id
										objet = item.objet
										item.name = nom
										item.image = image
										item.fichier = fichier
										item.x = item.x + 20
										item.y = item.y + 20
									})
									this.items[this.page].forEach(function (item, index) {
										if (item.objet === 'ancre') {
											this.items[this.page][index].visible = false
										}
									}.bind(this))
									this.$nextTick(function () {
										this.items[this.page].push(...items)
										this.nom = nom
										this.objet = objet
										this.enregistrer('ajouter', '', items)
										this.$nextTick(function () {
											const objets = this.$refs.transformer.getNode().getStage().find((item) => {
												return item.getAttrs().objet && item.getAttrs().objet === 'image'
											})
											for (let i = 0; i < objets.length; i++) {
												if (nom === objets[i].getAttrs().name) {
													if (filtre === 'Aucun') {
														objets[i].filters([])
													} else {
														objets[i].cache()
														objets[i].filters([Konva.Filters[filtre]])
														if (filtre === 'Pixelate') {
															objets[i].pixelSize(10)
														}
													}
												}
											}
											this.transformer()
											const copieItems = JSON.parse(JSON.stringify(items))
											copieItems.forEach(function (item) {
												if (item.objet === 'image') {
													delete item.image
												}
											})
											this.nom = ''
											this.objet = ''
											this.transformer()
											this.$nextTick(function () {
												this.nom = nom
												this.objet = objet
												this.transformer()
											}.bind(this))
											this.$socket.emit('ajouter', { tableau: this.tableau, page: this.page, items: copieItems })
										}.bind(this))
									}.bind(this))
								}.bind(this)
								image.onerror = function () {
									this.chargement = false
								}.bind(this)
							}
						}.bind(this)).catch(function () {
							this.message = this.$t('erreurCommunicationServeur')
						}.bind(this))
					} else if (fichier !== '' && filtre !== '' && this.verifierURL(fichier) === true) {
						const image = new window.Image()
						image.crossOrigin = 'Anonymous'
						image.src = fichier
						image.onload = function () {
							this.chargement = false
							items.forEach(function (item) {
								const type = item.name.substring(0, 4)
								nom = type + id
								objet = item.objet
								item.name = nom
								item.image = image
								item.fichier = fichier
								item.x = item.x + 20
								item.y = item.y + 20
							})
							this.items[this.page].forEach(function (item, index) {
								if (item.objet === 'ancre') {
									this.items[this.page][index].visible = false
								}
							}.bind(this))
							this.$nextTick(function () {
								this.items[this.page].push(...items)
								this.nom = nom
								this.objet = objet
								this.enregistrer('ajouter', '', items)
								this.$nextTick(function () {
									const objets = this.$refs.transformer.getNode().getStage().find((item) => {
										return item.getAttrs().objet && item.getAttrs().objet === 'image'
									})
									for (let i = 0; i < objets.length; i++) {
										if (nom === objets[i].getAttrs().name) {
											if (filtre === 'Aucun') {
												objets[i].filters([])
											} else {
												objets[i].cache()
												objets[i].filters([Konva.Filters[filtre]])
												if (filtre === 'Pixelate') {
													objets[i].pixelSize(10)
												}
											}
										}
									}
									this.transformer()
									const copieItems = JSON.parse(JSON.stringify(items))
									copieItems.forEach(function (item) {
										if (item.objet === 'image') {
											delete item.image
										}
									})
									this.nom = ''
									this.objet = ''
									this.transformer()
									this.$nextTick(function () {
										this.nom = nom
										this.objet = objet
										this.transformer()
									}.bind(this))
									this.$socket.emit('ajouter', { tableau: this.tableau, page: this.page, items: copieItems })
								}.bind(this))
							}.bind(this))
						}.bind(this)
						image.onerror = function () {
							this.chargement = false
						}.bind(this)
					}
				} else {
					itemsPage.forEach(function (item) {
						if (item.name.includes(this.nom)) {
							items.push(item)
						}
					}.bind(this))
					items.forEach(function (item) {
						const type = item.name.substring(0, 4)
						if (type === 'ancr') {
							item.name = item.name.substring(0, 9) + id + item.name.substring(item.name.length - 2)
							item.x = item.x + 20
							item.y = item.y + 20
						} else {
							nom = type + id
							objet = item.objet
							item.name = nom
							item.x = item.x + 20
							item.y = item.y + 20
						}
						if (type === 'labl') {
							item.tag.name = type + id
							item.text.name = type + id
						}
					})
					this.items[this.page].forEach(function (item, index) {
						if (item.objet === 'ancre') {
							this.items[this.page][index].visible = false
						}
					}.bind(this))
					this.$nextTick(function () {
						this.items[this.page].push(...items)
						this.nom = nom
						this.objet = objet
						this.enregistrer('ajouter', '', items)
						this.$nextTick(function () {
							this.transformer()
							const copieItems = JSON.parse(JSON.stringify(items))
							this.itemsDupliques.push(...copieItems)
							if (this.chrono !== '') {
								clearTimeout(this.chrono)
								this.chrono = ''
							}
							this.chrono = setTimeout(function () {
								this.$socket.emit('ajouter', { tableau: this.tableau, page: this.page, items: this.itemsDupliques })
								this.chrono = ''
								this.itemsDupliques = []
							}.bind(this), 400)
						}.bind(this))
					}.bind(this))
				}
			}
		},
		supprimer (mode) {
			const type = this.nom.substring(0, 4)
			const nom = this.nom
			if (type === 'imag') {
				this.imagesASupprimer.push(this.items[this.page].find(r => r.name === nom).fichier)
			}
			if (type === 'ancr') {
				this.items[this.page] = this.items[this.page].filter(r => !r.name.includes(nom.split('_')[1]))
			} else {
				this.items[this.page] = this.items[this.page].filter(r => !r.name.includes(nom))
			}
			this.nom = ''
			this.objet = ''
			this.transformer()
			if (type !== 'comm') {
				this.enregistrer('supprimer', nom, '')
			} else {
				this.$socket.emit('supprimercommentaires', { tableau: this.tableau, page: this.page, itemId: nom })
			}
			if (mode === 'objet') {
				this.$socket.emit('supprimer', { tableau: this.tableau, page: this.page, nom: nom })
			}
		},
		supprimerSelection () {
			const objets = this.$refs.transformer.getNode().nodes()
			const noms = []
			for (let i = 0; i < objets.length; i++) {
				const nom = objets[i].getAttrs().name
				const type = nom.substring(0, 4)
				if (type !== 'comm' || ((type === 'comm' && this.definirAuteurItem(nom) === this.identifiant) || this.admin)) {
					noms.push(objets[i].getAttrs().name)
					this.nom = objets[i].getAttrs().name
					this.supprimer('selection')
				}
			}
			this.$socket.emit('supprimerselection', { tableau: this.tableau, page: this.page, noms: noms })
		},
		afficherMenuPages () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.menu = 'pages'
			setTimeout(function () {
				document.querySelector('#menu-pages').classList.add('ouvert')
				document.querySelector('#menu-pages .page').focus()
			}, 0)
		},
		ajouterPage () {
			this.items.push([])
			this.enregistrer('ajouterpage', '', this.items.length - 1)
			this.$socket.emit('ajouterpage', { tableau: this.tableau, page: this.items.length - 1 })
		},
		afficherPage (page) {
			this.chargement = true
			this.reinitialiserSelection()
			this.page = page
			this.reinitialiserImages()
			this.chargerItems(this.items, this.page)
			const urlParams = new URLSearchParams(window.location.search)
			urlParams.set('page', this.page + 1)
			window.history.replaceState('', '', this.hote + '/b/' + this.tableau + '?' + urlParams)
			if (this.menu === 'pages') {
				document.querySelector('#page' + this.page).scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'center' })
			}
		},
		afficherPageSuivante () {
			this.chargement = true
			this.reinitialiserSelection()
			if (this.admin && this.page + 1 === this.items.length) {
				this.ajouterPage()
			} else if (!this.admin && this.page + 1 === this.items.length) {
				return false
			}
			this.page++
			this.reinitialiserImages()
			this.chargerItems(this.items, this.page)
			const urlParams = new URLSearchParams(window.location.search)
			urlParams.set('page', this.page + 1)
			window.history.replaceState('', '', this.hote + '/b/' + this.tableau + '?' + urlParams)
			if (this.menu === 'pages') {
				this.$nextTick(function () {
					document.querySelector('#page' + this.page).scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'center' })
				}.bind(this))
			}
		},
		afficherPagePrecedente () {
			if (this.page > 0) {
				this.chargement = true
				this.reinitialiserSelection()
				this.page--
				this.reinitialiserImages()
				this.chargerItems(this.items, this.page)
				const urlParams = new URLSearchParams(window.location.search)
				urlParams.set('page', this.page + 1)
				window.history.replaceState('', '', this.hote + '/b/' + this.tableau + '?' + urlParams)
				if (this.menu === 'pages') {
					this.$nextTick(function () {
						document.querySelector('#page' + this.page).scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'center' })
					}.bind(this))
				}
			}
		},
		deplacerPage (event) {
			this.page = event.newIndex
			this.afficherPage(this.page)
			this.$socket.emit('deplacerpage', { tableau: this.tableau, ancienIndex: event.oldIndex, nouvelIndex: event.newIndex })
		},
		supprimerPage (event, page) {
			event.preventDefault()
			event.stopPropagation()
			this.items[page].forEach(function (item) {
				if (item.objet === 'image') {
					this.imagesASupprimer.push(item.fichier)
				}
			}.bind(this))
			this.items.splice(page, 1)
			this.enregistrer('supprimerpage', '', page)
			this.$socket.emit('supprimerpage', { tableau: this.tableau, page: page })
			if (this.page === page && this.page > 0) {
				this.page--
				this.afficherPage(this.page)
			} else if (this.page === page && this.page === 0) {
				this.page = 0
				this.afficherPage(this.page)
			}
		},
		afficherMenuParametres () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.menu = 'parametres'
			setTimeout(function () {
				document.querySelector('#menu-parametres').classList.add('ouvert')
				document.querySelector('#menu-parametres .fermer').focus()
			}, 0)
		},
		fermerMenu () {
			this.menu = ''
			this.gererFocus()
		},
		gererClavierAdmin (event) {
			if (event.ctrlKey && event.key === 'd' && this.modale !== 'texte' && this.modale !== 'commentaire' && this.modale !== 'recherche') {
				event.preventDefault()
				this.dupliquer()
			} else if (event.ctrlKey && event.key === 'c' && this.modale !== 'texte' && this.modale !== 'commentaire' && this.modale !== 'recherche') {
				event.preventDefault()
				this.copier()
			} else if (event.ctrlKey && event.key === 'z' && this.modale !== 'texte' && this.modale !== 'commentaire' && this.modale !== 'recherche') {
				event.preventDefault()
				this.defaire()
			} else if (event.ctrlKey && event.key === 'y' && this.modale !== 'texte' && this.modale !== 'commentaire' && this.modale !== 'recherche') {
				event.preventDefault()
				this.refaire()
			} else if ((event.key === 'Backspace' || event.key === 'Delete') && this.modale !== 'texte' && this.modale !== 'commentaire' && this.modale !== 'recherche') {
				if (this.nom !== '' && this.objet !== 'selection' && (this.objet !== 'commentaire' || (this.objet === 'commentaire' && this.definirAuteurItem(this.nom) === this.identifiant || this.admin))) {
					this.supprimer('objet')
				} else if (this.nom !== '' && this.objet === 'selection') {
					this.supprimerSelection()
				}
			} else if ((event.key === 'ArrowDown' || event.key === 'ArrowUp' || event.key === 'ArrowLeft' || event.key === 'ArrowRight') && this.modale !== 'texte' && this.modale !== 'commentaire' && this.modale !== 'recherche') {
				const items = []
				let item, type, ancre1, ancre2
				if (this.objet !== 'selection') {
					item = this.items[this.page].find(r => r.name === this.nom)
					type = this.nom.substring(0, 4)
					if (type === 'flec' || type === 'line') {
						ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + this.nom + '_1')
						ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + this.nom + '_2')
					}
					if (item && event.key === 'ArrowDown') {
						item.y = item.y + 2
						if (type === 'flec' || type === 'line') {
							ancre1.y = ancre1.y + 2
							ancre2.y = ancre2.y + 2
						}
					} else if (item && event.key === 'ArrowUp') {
						item.y = item.y - 2
						if (type === 'flec' || type === 'line') {
							ancre1.y = ancre1.y - 2
							ancre2.y = ancre2.y - 2
						}
					} else if (item && event.key === 'ArrowLeft') {
						item.x = item.x - 2
						if (type === 'flec' || type === 'line') {
							ancre1.x = ancre1.x - 2
							ancre2.x = ancre2.x - 2
						}
					} else if (item && event.key === 'ArrowRight') {
						item.x = item.x + 2
						if (type === 'flec' || type === 'line') {
							ancre1.x = ancre1.x + 2
							ancre2.x = ancre2.x + 2
						}
					}
					items.push({ nom: this.nom, x: item.x, y: item.y })
					if (type === 'flec' || type === 'line') {
						items.push({ nom: 'ancr_' + this.nom + '_1', x: ancre1.x, y: ancre1.y })
						items.push({ nom: 'ancr_' + this.nom + '_2', x: ancre2.x, y: ancre2.y })
					}
				} else {
					const objets = this.$refs.transformer.getNode().nodes()
					for (let i = 0; i < objets.length; i++) {
						item = this.items[this.page].find(r => r.name === objets[i].getAttrs().name)
						type = objets[i].getAttrs().name.substring(0, 4)
						if (type === 'flec' || type === 'line') {
							ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + objets[i].getAttrs().name + '_1')
							ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + objets[i].getAttrs().name + '_2')
						}
						if (item && event.key === 'ArrowDown') {
							item.y = item.y + 2
							if (type === 'flec' || type === 'line') {
								ancre1.y = ancre1.y + 2
								ancre2.y = ancre2.y + 2
							}
						} else if (item && event.key === 'ArrowUp') {
							item.y = item.y - 2
							if (type === 'flec' || type === 'line') {
								ancre1.y = ancre1.y - 2
								ancre2.y = ancre2.y - 2
							}
						} else if (item && event.key === 'ArrowLeft') {
							item.x = item.x - 2
							if (type === 'flec' || type === 'line') {
								ancre1.x = ancre1.x - 2
								ancre2.x = ancre2.x - 2
							}
						} else if (item && event.key === 'ArrowRight') {
							item.x = item.x + 2
							if (type === 'flec' || type === 'line') {
								ancre1.x = ancre1.x + 2
								ancre2.x = ancre2.x + 2
							}
						}
						items.push({ nom: objets[i].getAttrs().name, x: item.x, y: item.y })
						if (type === 'flec' || type === 'line') {
							items.push({ nom: 'ancr_' + objets[i].getAttrs().name + '_1', x: ancre1.x, y: ancre1.y })
							items.push({ nom: 'ancr_' + objets[i].getAttrs().name + '_2', x: ancre2.x, y: ancre2.y })
						}
					}
				}
				if (this.chrono !== '') {
					clearTimeout(this.chrono)
					this.chrono = ''
				}
				this.chrono = setTimeout(function () {
					this.enregistrer('deplacer', '', items)
					this.$socket.emit('deplacer', { tableau: this.tableau, page: this.page, items: items })
					this.chrono = ''
				}.bind(this), 400)
			} else if (event.ctrlKey && event.key !== 'v' && this.modale !== 'texte' && this.modale !== 'commentaire' && this.modale !== 'recherche') {
				event.preventDefault()
				this.ctrl = true
			}
		},
		afficherReinitialiser () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'reinitialiser'
			this.$nextTick(function () {
				document.querySelector('.modale .bouton').focus()
			})
		},
		reinitialiser () {
			this.chargement = true
			this.modale = ''
			this.gererFocus()
			this.$socket.emit('reinitialiser', { tableau: this.tableau })
		},
		deplacerFin (event) {
			const items = []
			let item, type, ancre1, ancre2
			if (this.objet !== 'selection') {
				item = this.items[this.page].find(r => r.name === this.nom)
				type = this.nom.substring(0, 4)
				if (item && type === 'ancr') {
					const nom = this.nom.split('_')[1]
					item = this.items[this.page].find(r => r.name === nom)
					items.push({ nom: nom, x: item.x, y: item.y, points: item.points })
					ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + nom + '_1')
					ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + nom + '_2')
					items.push({ nom: 'ancr_' + nom + '_1', x: ancre1.x, y: ancre1.y })
					items.push({ nom: 'ancr_' + nom + '_2', x: ancre2.x, y: ancre2.y })
				} else if (item && (type === 'flec' || type === 'line')) {
					items.push({ nom: this.nom, x: item.x, y: item.y, points: item.points })
					ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + this.nom + '_1')
					ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + this.nom + '_2')
					items.push({ nom: 'ancr_' + this.nom + '_1', x: ancre1.x, y: ancre1.y })
					items.push({ nom: 'ancr_' + this.nom + '_2', x: ancre2.x, y: ancre2.y })
				} else if (item) {
					item.x = event.target.x()
					item.y = event.target.y()
					items.push({ nom: this.nom, x: event.target.x(), y: event.target.y() })
				}
			} else {
				const objets = this.$refs.transformer.getNode().nodes()
				for (let i = 0; i < objets.length; i++) {
					item = this.items[this.page].find(r => r.name === objets[i].getAttrs().name)
					type = objets[i].getAttrs().name.substring(0, 4)
					if (item && (type === 'flec' || type === 'line')) {
						items.push({ nom: objets[i].getAttrs().name, x: item.x, y: item.y })
						ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + objets[i].getAttrs().name + '_1')
						ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + objets[i].getAttrs().name + '_2')
						items.push({ nom: 'ancr_' + objets[i].getAttrs().name + '_1', x: ancre1.x, y: ancre1.y })
						items.push({ nom: 'ancr_' + objets[i].getAttrs().name + '_2', x: ancre2.x, y: ancre2.y })
					} else if (item) {
						item.x = objets[i].getAttrs().x
						item.y = objets[i].getAttrs().y
						items.push({ nom: objets[i].getAttrs().name, x: objets[i].getAttrs().x, y: objets[i].getAttrs().y })
					}
				}
			}
			this.enregistrer('deplacer', '', items)
			this.$socket.emit('deplacer', { tableau: this.tableau, page: this.page, items: items })
		},
		redimensionnerFin (event) {
			const item = this.items[this.page].find(r => r.name === this.nom)
			if (item) {
				item.x = event.target.x()
				item.y = event.target.y()
				item.width = event.target.width()
				item.height = event.target.height()
				item.rotation = event.target.rotation()
				item.scaleX = event.target.scaleX()
				item.scaleY = event.target.scaleY()
				this.enregistrer('redimensionner', this.nom, item)
				this.$socket.emit('redimensionner', { tableau: this.tableau, page: this.page, item: item })
			}
		},
		recupererPositionPointeur (stage) {
			const position = stage.getPointerPosition()
			const attributs = stage.attrs
			const x = (position.x - attributs.x) / attributs.scaleX
			const y = (position.y - attributs.y) / attributs.scaleY
			return { x: x, y: y }
		},
		selectionnerDebut (event) {
			const stage = event.target.getStage()
			if (this.outilSelectionner) {
				if (this.outil !== '' && this.creation === true) {
					this.selectionnerFin()
					return false
				}
				this.positionX1 = this.recupererPositionPointeur(stage).x
				this.positionY1 = this.recupererPositionPointeur(stage).y
				this.positionX2 = this.recupererPositionPointeur(stage).x
				this.positionY2 = this.recupererPositionPointeur(stage).y
				this.largeurSelection = 0
				this.hauteurSelection = 0
				this.largeurObjet = 0
				this.hauteurObjet = 0
				if (this.outil === '') {
					if (event.target.getParent() && event.target.getParent().className === 'Transformer') {
						this.itemsSelectionnes = []
						this.ctrl = false
						return false
					}
					if (event.target !== event.target.getStage()) {
						this.masquerAuteur()
						this.selectionnerObjet(event)
						return false
					} else {
						this.itemsSelectionnes = []
						this.ctrl = false
					}
					this.selection = true
					this.nom = ''
					this.objet = ''
					this.items[this.page].forEach(function (item, index) {
						if (item.objet === 'ancre') {
							this.items[this.page][index].visible = false
						}
					}.bind(this))
					this.transformer()
				} else {
					this.creation = true
					this.nom = ''
					this.objet = ''
					this.items[this.page].forEach(function (item, index) {
						if (item.objet === 'ancre') {
							this.items[this.page][index].visible = false
						}
					}.bind(this))
					if (this.outil === 'etoile') {
						this.dessinerForme('etoile')
						this.creation = false
						this.activerSelecteur()
					} else if (this.outil === 'texte' || this.outil === 'label') {
						this.elementPrecedent = (document.activeElement || document.body)
						this.modale = 'texte'
						this.creation = false
						setTimeout(function () {
							document.querySelector('#texte textarea').focus()
						}, 10)
					} else if (this.outil === 'commentaire') {
						this.elementPrecedent = (document.activeElement || document.body)
						this.modale = 'commentaire'
						this.creation = false
						this.$nextTick(function () {
							this.genererEditeur()
							document.querySelector('#editeur .contenu-editeur-commentaire').focus()
						}.bind(this))
					}
					this.transformer()
				}
			} else if (this.outilDessiner) {
				this.dessin = true
				this.positionStylo = this.recupererPositionPointeur(stage)
				const items = []
				const id = (new Date()).getTime() + Math.random().toString(16).slice(10)
				items.push({ name: 'dess' + id, objet: 'dessin', points: [this.positionStylo.x, this.positionStylo.y], globalCompositeOperation: 'source-over', stroke: this.couleurStylo, strokeWidth: this.epaisseurStylo, opacity: this.transparenceStylo, hitStrokeWidth: 25, lineJoin: 'round', draggable: false, verrouille: true })
				this.items[this.page].push(...items)
				this.itemsDessin.push(...items)
				this.nom = 'dess' + id
				this.objet = 'dessin'
				this.$nextTick(function () {
					this.$refs.objets.getNode().getLayer().batchDraw()
				}.bind(this))
			}
		},
		selectionnerMouvement (event) {
			const stage = event.target.getStage()
			if (this.outilSelectionner) {
				if (this.outil === '') {
					if (!this.selection) {
						return false
					}
					this.positionX2 = this.recupererPositionPointeur(stage).x
					this.positionY2 = this.recupererPositionPointeur(stage).y
					this.positionSelectionX = Math.min(this.positionX1, this.positionX2)
					this.positionSelectionY = Math.min(this.positionY1, this.positionY2)
					this.largeurSelection = Math.abs(this.positionX2 - this.positionX1)
					this.hauteurSelection = Math.abs(this.positionY2 - this.positionY1)
				} else {
					if (!this.creation) {
						return false
					}
					this.positionX2 = this.recupererPositionPointeur(stage).x
					this.positionY2 = this.recupererPositionPointeur(stage).y
					this.positionObjetX = Math.min(this.positionX1, this.positionX2)
					this.positionObjetY = Math.min(this.positionY1, this.positionY2)
					this.largeurObjet = Math.abs(this.positionX2 - this.positionX1)
					this.hauteurObjet = Math.abs(this.positionY2 - this.positionY1)
				}
				this.transformer()
			} else if (this.outilDessiner) {
				if (!this.dessin) {
					return false
				}
				this.positionStylo = this.recupererPositionPointeur(stage)
				const item = this.items[this.page].find(r => r.name === this.nom)
				const points = item.points.concat([this.positionStylo.x, this.positionStylo.y])
				item.points = points
				this.itemsDessin.forEach(function (dessin) {
					if (dessin.name === item.name) {
						dessin.points = points
					}
				})
				this.$refs.objets.getNode().getLayer().batchDraw()
			}
		},
		selectionnerFin () {
			if (this.outilSelectionner) {
				if (this.outil === '') {
					if (!this.selection) {
						return false
					}
					if (this.largeurSelection < 10 && this.hauteurSelection < 10) {
						this.selection = false
						return false
					}
					this.selection = false
					const transformer = this.$refs.transformer.getNode()
					const stage = transformer.getStage()
					const objets = stage.find((item) => {
						return item.getAttrs().objet && item.getAttrs().name.substring(0, 4) !== 'ancr' && item.getAttrs().objet !== 'dessin'
					})
					const rect = stage.findOne('.selection').getClientRect()
					const selection = objets.filter(item => Konva.Util.haveIntersection(rect, item.getClientRect()))
					if (selection.length > 1) {
						this.nom = 'selection'
						this.objet = 'selection'
						const objetsVerrouilles = selection.filter(item => item.getAttrs().verrouille === true)
						if (objetsVerrouilles.length === selection.length) {
							this.objetVerrouille = true
						} else {
							this.objetVerrouille = false
						}
						selection.forEach(function (item) {
							this.itemsSelectionnes.push(item.getAttrs().name)
						}.bind(this))
						transformer.nodes(selection)
						transformer.getLayer().batchDraw()
						this.desactiverSelecteur()
					} else if (selection.length === 1) {
						const nom = selection[0].getAttrs().name
						this.selectionner(nom)
						this.transformer()
					} else {
						this.nom = ''
						this.objet = ''
						this.objetVerrouille = false
						this.itemsSelectionnes = []
						this.ctrl = false
						this.desactiverSelecteur()
					}
				} else if (this.outil !== '' && this.outil !== 'etoile' && this.outil !== 'texte' && this.outil !== 'label' && this.outil !== 'commentaire' && this.largeurObjet > 0 && this.hauteurObjet > 0) {
					if (!this.creation) {
						return false
					}
					this.dessinerForme(this.outil)
					this.desactiverSelecteur()
				}
			} else if (this.outilDessiner) {
				this.dessin = false
				if (this.itemsDessin.length > 0) {
					this.enregistrer('ajouter', '', this.itemsDessin)
					this.$socket.emit('ajouter', { tableau: this.tableau, page: this.page, items: this.itemsDessin })
					this.itemsDessin = []
				}
				this.nom = ''
				this.objet = ''
			}
		},
		selectionnerObjet (event) {
			if (!this.ctrl && this.outilSelectionner) {
				if (event.target === event.target.getStage()) {
					this.reinitialiserSelection()
					return false
				}
				if (this.objet === 'selection' && this.itemsSelectionnes.includes(event.target.name()) && event.target.getAttrs().objet && event.target.getAttrs().name.substring(0, 4) !== 'ancr') {
					return false
				} else if (this.objet === 'selection' && !this.itemsSelectionnes.includes(event.target.name()) && event.target.getAttrs().objet && event.target.getAttrs().name.substring(0, 4) !== 'ancr') {
					this.reinitialiserSelection()
				}
				if (event.target.getParent().className === 'Transformer') {
					this.reinitialiserSelection()
					return false
				}
				this.selectionner(event.target.name())
				this.transformer()
			} else if (this.ctrl && this.outilSelectionner) {
				const transformer = this.$refs.transformer.getNode()
				const stage = transformer.getStage()
				const objets = stage.find((item) => {
					return item.getAttrs().objet && item.getAttrs().name.substring(0, 4) !== 'ancr' && item.getAttrs().objet !== 'dessin'
				})
				if (this.nom !== '') {
					this.itemsSelectionnes.push(this.nom, event.target.name())
				} else if (this.nom === 'selection') {
					this.itemsSelectionnes.push(event.target.name())
				}
				const selection = objets.filter(function (item) {
					return this.itemsSelectionnes.includes(item.getAttrs().name)
				}.bind(this))
				if (selection.length > 1) {
					this.nom = 'selection'
					this.objet = 'selection'
					const objetsVerrouilles = selection.filter(item => item.getAttrs().verrouille === true)
					if (objetsVerrouilles.length === selection.length) {
						this.objetVerrouille = true
					} else {
						this.objetVerrouille = false
					}
					transformer.nodes(selection)
					transformer.getLayer().batchDraw()
					this.desactiverSelecteur()
				} else {
					if (event.target === event.target.getStage()) {
						this.reinitialiserSelection()
						return false
					}
					if (this.objet === 'selection' && this.itemsSelectionnes.includes(event.target.name()) && event.target.getAttrs().objet && event.target.getAttrs().name.substring(0, 4) !== 'ancr') {
						return false
					} else if (this.objet === 'selection' && !this.itemsSelectionnes.includes(event.target.name()) && event.target.getAttrs().objet && event.target.getAttrs().name.substring(0, 4) !== 'ancr') {
						this.reinitialiserSelection()
					}
					if (event.target.getParent().className === 'Transformer') {
						this.reinitialiserSelection()
						return false
					}
					this.selectionner(event.target.name())
					this.transformer()
				}
			}
		},
		selectionner (nom) {
			let ancre1, ancre2
			const type = nom.substring(0, 4)
			const item = this.items[this.page].find(r => r.name === nom)
			if (item) {
				this.nom = nom
				this.objet = item.objet
				if (!item.draggable) {
					this.objetVerrouille = true
				} else {
					this.objetVerrouille = false
				}
				if (!this.outilDessiner) {
					this.definirCouleurSelecteur(this.objet, item)
				}
				this.activerSelecteur()
			} else {
				this.nom = ''
				this.objet = ''
				this.objetVerrouille = false
				this.itemsSelectionnes = []
				this.ctrl = false
				this.desactiverSelecteur()
			}
			if (type === 'flec' || type === 'line') {
				this.items[this.page].forEach(function (item, index) {
					if (item.objet === 'ancre') {
						this.items[this.page][index].visible = false
					}
				}.bind(this))
				ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + nom + '_1')
				ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + nom + '_2')
				if (!item.verrouille) {
					ancre1.visible = true
					ancre2.visible = true
				} else {
					ancre1.visible = false
					ancre2.visible = false
				}
			} else if (type !== 'ancr') {
				this.items[this.page].forEach(function (item, index) {
					if (item.objet === 'ancre') {
						this.items[this.page][index].visible = false
					}
				}.bind(this))
			}
		},
		afficherAuteur (event) {
			if (this.options.affichageAuteur === 'non' || event.target === event.target.getStage() || (event.target.getParent().className === 'Transformer')) {
				return false
			}
			const item = this.items[this.page].find(r => r.name === event.target.name())
			let auteur = ''
			this.utilisateurs.forEach(function (utilisateur) {
				if (item.hasOwnProperty('identifiant') && utilisateur.identifiant === item.identifiant) {
					auteur = utilisateur.nom
				}
			})
			if (auteur !== '') {
				const deltaLargeur = document.querySelector('#tableau .konvajs-content').getBoundingClientRect().left
				const deltaHauteur = document.querySelector('#tableau .konvajs-content').getBoundingClientRect().top
				const rect = event.target.getClientRect({ skipTransform: false })
				let top = ((rect.y + rect.height) * this.echelle) + deltaHauteur
				let left = (rect.x * this.echelle) + deltaLargeur
				if (top > (deltaHauteur + (document.querySelector('#tableau .konvajs-content').offsetHeight * this.echelle))) {
					top = ((rect.y * this.echelle) + deltaHauteur) - 17
				}
				if (left < deltaLargeur) {
					left = deltaLargeur
				}
				const element = document.querySelector('#auteur')
				element.textContent = auteur
				element.style.top = top + 'px'
				element.style.left = left + 'px'
				element.style.display = 'block'
			}
		},
		masquerAuteur () {
			const element = document.querySelector('#auteur')
			element.textContent = ''
			element.style.display = 'none'
		},
		definirAuteurItem (nom) {
			const item = this.items[this.page].find(r => r.name === nom)
			if (item && item.hasOwnProperty('identifiant') === true) {
				return item.identifiant
			} else {
				return ''
			}
		},
		transformer () {
			const transformer = this.$refs.transformer.getNode()
			const stage = transformer.getStage()
			const noeud = stage.findOne('.' + this.nom)
			if (noeud && this.nom.substring(0, 4) !== 'ancr') {
				transformer.nodes([noeud])
			} else {
				transformer.nodes([])
			}
			transformer.getLayer().batchDraw()
		},
		reinitialiserOutils () {
			this.outil = ''
			this.largeurObjet = 0
			this.hauteurObjet = 0
			this.positionX1 = 0
			this.positionY1 = 0
			this.positionX2 = 0
			this.positionY2 = 0
		},
		reinitialiserSelection () {
			this.nom = ''
			this.objet = ''
			this.objetVerrouille = false
			this.itemsSelectionnes = []
			this.ctrl = false
			this.items[this.page].forEach(function (item, index) {
				if (item.objet === 'ancre') {
					this.items[this.page][index].visible = false
				}
			}.bind(this))
			this.transformer()
		},
		reinitialiserImages () {
			this.items.forEach(function (page, indexPage) {
				page.forEach(function (item, indexItem) {
					if (item.objet === 'image' && item.hasOwnProperty('image') === true) {
						delete this.items[indexPage][indexItem].image
					}
				}.bind(this))
			}.bind(this))
		},
		definirAncres () {
			let ancres = []
			if (this.objetVerrouille === false) {
				const type = this.nom.substring(0, 4)
				switch (type) {
				case 'rect':
				case 'circ':
					ancres = ['top-left', 'top-center', 'top-right', 'middle-right', 'middle-left', 'bottom-left', 'bottom-center', 'bottom-right']
					break
				case 'labl':
				case 'imag':
				case 'star':
					ancres = ['top-left', 'top-right', 'bottom-left', 'bottom-right']
					break
				case 'comm':
				case 'text':
					ancres = []
					break
				}
			}
			return ancres
		},
		definirCouleurBordure () {
			if (!this.admin && this.role !== 'editeur' && this.options.edition !== 'ouverte') {
				return 'transparent'
			} else if (!this.objetVerrouille) {
				return '#01ced1'
			} else {
				return '#aaaaaa'
			}
		},
		definirRotation () {
			if (this.items[this.page]) {
				const item = this.items[this.page].find(r => r.name === this.nom)
				if (!item || this.objet === 'selection' || this.objet === 'fleche' || this.objet === 'ligne' || this.objet === 'commentaire' || (item && item.draggable === false) || (item && item.verrouille === true)) {
					return false
				} else {
					return true
				}
			} else {
				return false
			}
		},
		definirLimites (ancien, nouveau) {
			const type = this.nom.substring(0, 4)
			if (type === 'text') {
				if (nouveau.width < 70) {
					return ancien
				}
				return nouveau
			} else {
				return nouveau
			}
		},
		deplacerLigne (event) {
			let item, ancre1, ancre2
			if (this.objet !== 'selection') {
				item = this.items[this.page].find(r => r.name === this.nom)
				if (item) {
					item.points = event.target.points()
					item.x = event.target.x()
					item.y = event.target.y()
					item.scaleX = event.target.scaleX()
					item.scaleY = event.target.scaleY()
					ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + this.nom + '_1')
					ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + this.nom + '_2')
					ancre1.x = event.target.points()[0] + event.target.x()
					ancre1.y = event.target.points()[1] + event.target.y()
					ancre1.scaleX = event.target.scaleX()
					ancre1.scaleY = event.target.scaleY()
					ancre2.x = event.target.points()[2] + event.target.x()
					ancre2.y = event.target.points()[3] + event.target.y()
					ancre2.scaleX = event.target.scaleX()
					ancre2.scaleY = event.target.scaleY()
				}
			} else {
				const objets = this.$refs.transformer.getNode().nodes()
				for (let i = 0; i < objets.length; i++) {
					if (objets[i].getAttrs().name.substring(0, 4) === 'flec' || objets[i].getAttrs().name.substring(0, 4) === 'line') {
						item = this.items[this.page].find(r => r.name === objets[i].getAttrs().name)
					}
					if (item) {
						item.points = objets[i].getAttrs().points
						item.x = objets[i].getAttrs().x
						item.y = objets[i].getAttrs().y
						item.scaleX = objets[i].getAttrs().scaleX
						item.scaleY = objets[i].getAttrs().scaleY
						ancre1 = this.items[this.page].find(r => r.name === 'ancr_' + objets[i].getAttrs().name + '_1')
						ancre2 = this.items[this.page].find(r => r.name === 'ancr_' + objets[i].getAttrs().name + '_2')
						ancre1.x = objets[i].getAttrs().points[0] + objets[i].getAttrs().x
						ancre1.y = objets[i].getAttrs().points[1] + objets[i].getAttrs().y
						ancre1.scaleX = objets[i].getAttrs().scaleX
						ancre1.scaleY = objets[i].getAttrs().scaleY
						ancre2.x = objets[i].getAttrs().points[2] + objets[i].getAttrs().x
						ancre2.y = objets[i].getAttrs().points[3] + objets[i].getAttrs().y
						ancre2.scaleX = objets[i].getAttrs().scaleX
						ancre2.scaleY = objets[i].getAttrs().scaleY
					}
					item = null
				}
			}
		},
		deplacerAncre (event) {
			let nom, index, item, ancre, points, ancreActive
			if (this.objet !== 'selection') {
				nom = this.nom.split('_')
				index = parseInt(nom[2])
				item = this.items[this.page].find(r => r.name === nom[1])
				if (item) {
					if (index === 1) {
						ancre = this.items[this.page].find(r => r.name === 'ancr_' + nom[1] + '_2')
						points = [
							event.target.x() - item.x,
							event.target.y() - item.y,
							ancre.x - item.x,
							ancre.y - item.y
						]
					} else {
						ancre = this.items[this.page].find(r => r.name === 'ancr_' + nom[1] + '_1')
						points = [
							ancre.x - item.x,
							ancre.y - item.y,
							event.target.x() - item.x,
							event.target.y() - item.y
						]
					}
					item.points = points
					ancreActive = this.items[this.page].find(r => r.name === 'ancr_' + nom[1] + '_' + index)
					ancreActive.x = event.target.x()
					ancreActive.y = event.target.y()
					ancreActive.scaleX = event.target.scaleX()
					ancreActive.scaleY = event.target.scaleY()
				}
			} else {
				const objets = this.$refs.transformer.getNode().nodes()
				for (let i = 0; i < objets.length; i++) {
					nom = objets[i].getAttrs().name.split('_')
					index = parseInt(nom[2])
					if (nom[1].substring(0, 4) === 'flec' || nom[1].substring(0, 4) === 'line') {
						item = this.items[this.page].find(r => r.name === nom[1])
					}
					if (item) {
						if (index === 1) {
							ancre = this.items[this.page].find(r => r.name === 'ancr_' + nom[1] + '_2')
							points = [
								objets[i].getAttrs().x - item.x,
								objets[i].getAttrs().y - item.y,
								ancre.x - item.x,
								ancre.y - item.y
							]
						} else {
							ancre = this.items[this.page].find(r => r.name === 'ancr_' + nom[1] + '_1')
							points = [
								ancre.x - item.x,
								ancre.y - item.y,
								objets[i].getAttrs().x - item.x,
								objets[i].getAttrs().y - item.y
							]
						}
						item.points = points
						ancreActive = this.items[this.page].find(r => r.name === 'ancr_' + nom[1] + '_' + index)
						ancreActive.x = objets[i].getAttrs().x
						ancreActive.y = objets[i].getAttrs().y
						ancreActive.scaleX = objets[i].getAttrs().scaleX
						ancreActive.scaleY = objets[i].getAttrs().scaleY
					}
				}
			}
		},
		exporterImage () {
			this.modale = ''
			this.reinitialiserSelection()
			this.chargement = true
			setTimeout(function () {
				const echelle = 1920 / (1920 * this.echelle)
				html2canvas(document.querySelector('.konvajs-content'), { useCORS: true, scale: echelle, windowWidth: 1880, windowHeight: 1040 }).then(function (canvas) {
					saveAs(canvas.toDataURL('image/jpeg', 0.95).replace('image/jpeg', 'image/octet-stream'), this.$t('tableau') + '_' + (new Date()).getTime() + '.jpg')
					this.chargement = false
					this.notification = this.$t('tableauExporteImage')
				}.bind(this))
			}.bind(this), 10)
		},
		exporterTableau () {
			this.modale = ''
			this.chargement = true
			axios.post(this.hote + '/api/exporter-tableau', {
				tableau: this.tableau,
				items: this.items,
				commentaires: this.commentaires,
				fond: this.options.fond
			}).then(function (reponse) {
				const donnees = reponse.data
				if (donnees === 'non_autorise') {
					this.message = this.$t('actionNonAutorisee')
				} else {
					saveAs('/temp/' + donnees, 'tableau-' + this.tableau + '.zip')
					this.notification = this.$t('tableauExporte')
					this.chargement = false
				}
			}.bind(this)).catch(function () {
				this.chargement = false
				this.message = this.$t('erreurCommunicationServeur')
			}.bind(this))
		},
		afficherImporter () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'importer'
			this.$nextTick(function () {
				document.querySelector('.modale .fermer').focus()
			})
		},
		importerTableau (event) {
			const fichier = event.target.files[0]
			if (fichier === null || fichier.length === 0) {
				document.querySelector('#importer').value = ''
				return false
			} else {
				const extension = fichier.name.substring(fichier.name.lastIndexOf('.') + 1).toLowerCase()
				if (extension === 'zip') {
					this.menu = ''
					this.modale = ''
					this.chargement = true
					const formulaire = new FormData()
					formulaire.append('tableau', this.tableau)
					formulaire.append('fichier', fichier)
					axios.post(this.hote + '/api/importer-tableau', formulaire, {
						headers: {
							'Content-Type': 'multipart/form-data'
						}
					}).then(function (reponse) {
						const donnees = reponse.data
						if (donnees === 'non_autorise') {
							this.chargement = false
							this.message = this.$t('actionNonAutorisee')
						} else if (donnees === 'erreur_import') {
							this.chargement = false
							this.message = this.$t('erreurCommunicationServeur')
						} else {
							this.reinitialiserSelection()
							this.page = 0
							this.historique = []
							this.positionHistorique = 0
							this.positionActuelleHistorique = 0
							this.donnees = donnees.items
							this.commentaires = donnees.commentaires
							this.options.fond = donnees.fond
							if (donnees.fond.substring(0, 1) === '#') {
								document.querySelector('#tableau .konvajs-content').style.backgroundColor = donnees.fond
								document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'none'
							} else {
								document.querySelector('#tableau .konvajs-content').style.backgroundColor = 'transparent'
								document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'url(/img/' + donnees.fond + ')'
								document.querySelector('#tableau .konvajs-content').style.backgroundRepeat = 'repeat'
							}
							this.reinitialiserImages()
							this.chargerItems(donnees.items, this.page)
							this.$socket.emit('importer', { tableau: this.tableau, items: donnees.items, commentaires: donnees.commentaires, fond: donnees.fond })
							this.notification = this.$t('tableauImporte')
						}
					}.bind(this)).catch(function () {
						this.chargement = false
						this.message = this.$t('erreurCommunicationServeur')
					}.bind(this))
				} else {
					this.notification = this.$t('formatFichierPasAccepte')
					document.querySelector('#importer').value = ''
				}
			}
		},
		enregistrer (type, nom, nouveau) {
			const donnees = { page: this.page, items: JSON.parse(JSON.stringify(this.items)), type: type, nom: nom, nouveau: nouveau }
			if (!this.historique.includes(donnees) && this.positionActuelleHistorique === 0) {
				this.historique.push(donnees)
				this.positionHistorique += 1
			} else if (!this.historique.includes(donnees) && this.positionActuelleHistorique !== 0) {
				const historique = []
				for (let i = 0; i < this.positionActuelleHistorique; i++) {
					historique.push(this.historique[i])
				}
				historique.push(donnees)
				this.historique = historique
				this.positionHistorique = this.positionActuelleHistorique + 1
				this.positionActuelleHistorique = 0
			}
		},
		defaire () {
			if (this.positionHistorique === 0) {
				return false
			}
			this.positionHistorique -= 1
			this.positionActuelleHistorique = this.positionHistorique
			const donnees = this.historique[this.positionHistorique]
			let itemsPrecedents
			if (this.positionHistorique === 0) {
				itemsPrecedents = JSON.parse(JSON.stringify(this.donnees))
			} else {
				itemsPrecedents = JSON.parse(JSON.stringify(this.historique[this.positionHistorique - 1].items))
			}
			this.page = donnees.page
			let indexPrecedent
			let items = []
			const itemPrecedent = {}
			let objets = []
			let obj
			let erreur = false
			const images = []
			this.reinitialiserSelection()
			switch (donnees.type) {
			case 'ajouter':
				donnees.nouveau.forEach(function (item) {
					if (item.name.substring(0, 4) === 'imag') {
						this.imagesASupprimer.push(this.items[this.page].find(r => r.name === item.name).fichier)
					}
					if (item.name.substring(0, 4) === 'ancr') {
						this.items[this.page] = this.items[this.page].filter(r => !r.name.includes(item.name.split('_')[1]))
					} else {
						this.items[this.page] = this.items[this.page].filter(r => !r.name.includes(item.name))
					}
					this.$socket.emit('supprimer', { tableau: this.tableau, page: this.page, nom: item.name })
				}.bind(this))
				this.transformer()
				break
			case 'supprimer':
				itemsPrecedents[this.page].forEach(function (item, index) {
					if (item.name.includes(donnees.nom) === true) {
						item.index = index
						items.push(item)
					}
				})
				items = items.filter(function (element, index, arr) {
					return arr.map(obj => obj.name).indexOf(element.name) === index
				})
				items.forEach(function (item) {
					if (item.hasOwnProperty('fichier') && item.fichier !== '') {
						this.imagesASupprimer.forEach(function (image, index) {
							if (image === item.fichier) {
								this.imagesASupprimer.splice(index, 1)
							}
						}.bind(this))
					}
					if (item.verrouille === false && item.draggable === false) {
						item.draggble = true
					}
				}.bind(this))
				if (items[0].objet === 'image' && items[0].fichier !== '') {
					const image = new window.Image()
					if (this.verifierURL(items[0].fichier) === false) {
						image.src = '/fichiers/' + this.tableau + '/' + items[0].fichier
					} else {
						image.crossOrigin = 'Anonymous'
						image.src = items[0].fichier
					}
					image.onload = function () {
						items[0].image = image
						this.items[this.page].splice(...[items[0].index, 0].concat(items))
						this.$nextTick(function () {
							this.transformer()
							this.$forceUpdate()
							const copieItems = JSON.parse(JSON.stringify(items))
							copieItems.forEach(function (item) {
								if (item.objet === 'image') {
									delete item.image
								}
								if (item.hasOwnProperty('index')) {
									delete item.index
								}
							})
							this.$nextTick(function () {
								objets = this.$refs.transformer.getNode().getStage().find((item) => {
									return item.getAttrs().objet && item.getAttrs().objet === 'image'
								})
								for (let i = 0; i < objets.length; i++) {
									if (items[0].name === objets[i].getAttrs().name) {
										if (items[0].filtre === 'Aucun') {
											objets[i].filters([])
										} else {
											objets[i].cache()
											objets[i].filters([Konva.Filters[items[0].filtre]])
											if (items[0].filtre === 'Pixelate') {
												objets[i].pixelSize(10)
											}
										}
									}
								}
								this.transformer()
								this.$socket.emit('reajouter', { tableau: this.tableau, page: this.page, items: copieItems, index: items[0].index })
							}.bind(this))
						}.bind(this))
					}.bind(this)
				} else if (items[0].objet !== 'image') {
					items.forEach(function (item) {
						if (item.objet === 'ancre') {
							item.visible = false
						}
					})
					this.items[this.page].splice(...[items[0].index, 0].concat(items))
					this.$nextTick(function () {
						this.transformer()
						this.$forceUpdate()
						const copieItems = JSON.parse(JSON.stringify(items))
						copieItems.forEach(function (item) {
							if (item.objet === 'image') {
								delete item.image
							}
							if (item.hasOwnProperty('index')) {
								delete item.index
							}
						})
						this.$socket.emit('reajouter', { tableau: this.tableau, page: this.page, items: copieItems, index: items[0].index })
					}.bind(this))
				}
				break
			case 'verrouiller':
				donnees.nom.forEach(function (nom) {
					this.items[this.page].forEach(function (item) {
						if (item.name === nom) {
							item.draggable = true
							item.verrouille = false
						}
					})
				}.bind(this))
				this.$socket.emit('deverrouiller', { tableau: this.tableau, page: this.page, noms: donnees.nom })
				break
			case 'deverrouiller':
				donnees.nom.forEach(function (nom) {
					this.items[this.page].forEach(function (item) {
						if (item.name === nom) {
							item.draggable = false
							item.verrouille = true
						}
					})
				}.bind(this))
				this.$socket.emit('verrouiller', { tableau: this.tableau, page: this.page, noms: donnees.nom })
				break
			case 'modifierposition':
				itemsPrecedents[this.page].forEach(function (item, index) {
					if (item.name === donnees.nom) {
						indexPrecedent = index
					}
				})
				this.items[this.page].forEach(function (item, index) {
					if (item.name === donnees.nom) {
						this.items[this.page].splice(index, 1)
						this.items[this.page].splice(indexPrecedent, 0, item)
					}
				}.bind(this))
				this.$socket.emit('modifierposition', { tableau: this.tableau, page: this.page, nom: this.nom, index: indexPrecedent })
				break
			case 'deplacer':
				donnees.nouveau.forEach(function (item) {
					itemsPrecedents[this.page].forEach(function (itemP) {
						if (item.nom === itemP.name) {
							items.push({ nom: itemP.name, x: itemP.x, y: itemP.y })
							if (item.hasOwnProperty('points')) {
								items.push({ nom: itemP.name, x: itemP.x, y: itemP.y, points: itemP.points })
							}
						}
					})
				}.bind(this))
				items.forEach(function (itemP) {
					this.items[this.page].forEach(function (item) {
						if (item.name === itemP.nom) {
							item.x = itemP.x
							item.y = itemP.y
							if (itemP.hasOwnProperty('points')) {
								item.points = itemP.points
							}
						}
					})
				}.bind(this))
				this.$socket.emit('deplacer', { tableau: this.tableau, page: this.page, items: items })
				break
			case 'redimensionner':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						itemPrecedent.name = donnees.nom
						itemPrecedent.x = itemP.x
						itemPrecedent.y = itemP.y
						itemPrecedent.width = itemP.width
						itemPrecedent.height = itemP.height
						itemPrecedent.rotation = itemP.rotation
						itemPrecedent.scaleX = itemP.scaleX
						itemPrecedent.scaleY = itemP.scaleY
					}
				})
				this.items[this.page].forEach(function (item) {
					if (item.name === itemPrecedent.name) {
						item.x = itemPrecedent.x
						item.y = itemPrecedent.y
						item.width = itemPrecedent.width
						item.height = itemPrecedent.height
						item.rotation = itemPrecedent.rotation
						item.scaleX = itemPrecedent.scaleX
						item.scaleY = itemPrecedent.scaleY
					}
				})
				this.$socket.emit('redimensionner', { tableau: this.tableau, page: this.page, item: itemPrecedent })
				break
			case 'modifiertransparence':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						itemPrecedent.transparence = itemP.opacity
					}
				})
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					obj.opacity = itemPrecedent.transparence
					this.$socket.emit('modifiertransparence', { tableau: this.tableau, page: this.page, nom: donnees.nom, transparence: itemPrecedent.transparence })
				} else {
					erreur = true
				}
				break
			case 'modifiertexte':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						itemPrecedent.texte = itemP.text
					}
				})
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					if (donnees.nom.substring(0, 4) === 'text') {
						obj.text = itemPrecedent.texte
					} else {
						obj.text.text = itemPrecedent.texte
					}
					this.$socket.emit('modifiertexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, texte: itemPrecedent.texte })
				} else {
					erreur = true
				}
				break
			case 'modifiertailletexte':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						itemPrecedent.taille = itemP.fontSize
					}
				})
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					obj.fontSize = itemPrecedent.taille
					this.$socket.emit('modifiertailletexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, taille: itemPrecedent.taille })
				} else {
					erreur = true
				}
				break
			case 'modifieralignementtexte':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						itemPrecedent.alignement = itemP.align
					}
				})
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					obj.align = itemPrecedent.alignement
					obj.opacity = 0.9
					obj.opacity = 1
					this.$socket.emit('modifieralignementtexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, alignement: itemPrecedent.alignement })
				} else {
					erreur = true
				}
				break
			case 'modifierstyletexte':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						itemPrecedent.style = itemP.fontStyle
					}
				})
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					if (obj.objet === 'label') {
						obj = obj.text
					}
					obj.fontStyle = itemPrecedent.style
					this.$socket.emit('modifierstyletexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, style: itemPrecedent.style })
				} else {
					erreur = true
				}
				break
			case 'modifierdecorationtexte':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						itemPrecedent.decoration = itemP.textDecoration
					}
				})
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					if (obj.objet === 'label') {
						obj = obj.text
					}
					obj.textDecoration = itemPrecedent.decoration
					this.$socket.emit('modifierdecorationtexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, decoration: itemPrecedent.decoration })
				} else {
					erreur = true
				}
				break
			case 'modifierfiltre':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						itemPrecedent.filtre = itemP.filtre
					}
				})
				objets = this.$refs.transformer.getNode().getStage().find((item) => {
					return item.getAttrs().objet && item.getAttrs().objet === 'image'
				})
				for (let i = 0; i < objets.length; i++) {
					if (donnees.nom === objets[i].getAttrs().name) {
						if (itemPrecedent.filtre === 'Aucun') {
							objets[i].filters([])
						} else {
							objets[i].cache()
							objets[i].filters([Konva.Filters[itemPrecedent.filtre]])
							if (itemPrecedent.filtre === 'Pixelate') {
								objets[i].pixelSize(10)
							}
						}
					}
				}
				this.$socket.emit('modifierfiltre', { tableau: this.tableau, page: this.page, nom: donnees.nom, filtre: itemPrecedent.filtre })
				break
			case 'modifiercouleur':
				itemsPrecedents[this.page].forEach(function (itemP) {
					if (donnees.nom === itemP.name) {
						if (itemP.objet === 'rectangle' || itemP.objet === 'cercle' || itemP.objet === 'dessin') {
							itemPrecedent.couleur = itemP.stroke
						} else if (itemP.objet === 'rectangle-plein' || itemP.objet === 'cercle-plein' || itemP.objet === 'etoile' || itemP.objet === 'surlignage' || itemP.objet === 'texte' || itemP.objet === 'ligne' || itemP.objet === 'fleche' || itemP.objet === 'ancre') {
							itemPrecedent.couleur = itemP.fill
						} else if (itemP.objet === 'label' || itemP.objet === 'commentaire') {
							itemPrecedent.couleur = itemP.tag.fill
						}
					}
				})
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					if (obj.objet === 'rectangle' || obj.objet === 'cercle' || obj.objet === 'dessin') {
						obj.stroke = itemPrecedent.couleur
					} else if (obj.objet === 'rectangle-plein' || obj.objet === 'cercle-plein' || obj.objet === 'etoile' || obj.objet === 'surlignage' || obj.objet === 'texte') {
						obj.fill = itemPrecedent.couleur
					} else if (obj.objet === 'ligne' || obj.objet === 'fleche' || obj.objet === 'ancre') {
						obj.stroke = itemPrecedent.couleur
						obj.fill = itemPrecedent.couleur
					} else if (obj.objet === 'label' || obj.objet === 'commentaire') {
						obj.tag.fill = itemPrecedent.couleur
					}
					this.$socket.emit('modifiercouleur', { tableau: this.tableau, page: this.page, nom: donnees.nom, couleur: itemPrecedent.couleur })
				} else {
					erreur = true
				}
				break
			case 'ajouterpage':
				this.items.splice(donnees.nouveau, 1)
				this.$socket.emit('supprimerpage', { tableau: this.tableau, page: donnees.nouveau })
				if (this.page === (donnees.nouveau - 1) && this.page > 0) {
					this.afficherPage(this.page)
				} else if (this.page === (donnees.nouveau - 1) && this.page === 0) {
					this.page = 0
					this.afficherPage(this.page)
				}
				break
			case 'supprimerpage':
				itemsPrecedents[donnees.nouveau].forEach(function (item, index) {
					if (item.name.includes(donnees.nom) === true) {
						item.index = index
						items.push(item)
					}
				})
				items = items.filter(function (element, index, arr) {
					return arr.map(obj => obj.name).indexOf(element.name) === index
				})
				items.forEach(function (item) {
					if (item.hasOwnProperty('fichier') && item.fichier !== '') {
						this.imagesASupprimer.forEach(function (image, index) {
							if (image === item.fichier) {
								this.imagesASupprimer.splice(index, 1)
							}
						}.bind(this))
					}
					if (item.verrouille === false && item.draggable === false) {
						item.draggble = true
					}
				}.bind(this))
				for (let i = 0; i < items.length; i++) {
					if (items[i].objet === 'image' && items[i].fichier !== '') {
						const donneesImage = new Promise(function (resolve) {
							const image = new window.Image()
							if (this.verifierURL(items[i].fichier) === false) {
								image.src = '/fichiers/' + this.tableau + '/' + items[i].fichier
							} else {
								image.crossOrigin = 'Anonymous'
								image.src = items[i].fichier
							}
							image.onload = function () {
								items[i].image = image
								resolve('termine')
							}
							image.onerror = function () {
								resolve()
							}
						}.bind(this))
						images.push(donneesImage)
					} else {
						images.push('termine')
					}
				}
				Promise.all(images).then(function () {
					items.forEach(function (item) {
						if (item.objet === 'ancre') {
							item.visible = false
						}
					})
					this.items.splice(donnees.nouveau, 0, items)
					this.$nextTick(function () {
						this.transformer()
						this.$forceUpdate()
						const copieItems = JSON.parse(JSON.stringify(items))
						copieItems.forEach(function (item) {
							if (item.objet === 'image') {
								delete item.image
							}
							if (item.hasOwnProperty('index')) {
								delete item.index
							}
						})
						this.$nextTick(function () {
							objets = this.$refs.transformer.getNode().getStage().find((item) => {
								return item.getAttrs().objet && item.getAttrs().objet === 'image'
							})
							for (let i = 0; i < objets.length; i++) {
								if (items[0].name === objets[i].getAttrs().name) {
									if (items[0].filtre === 'Aucun') {
										objets[i].filters([])
									} else {
										objets[i].cache()
										objets[i].filters([Konva.Filters[items[0].filtre]])
										if (items[0].filtre === 'Pixelate') {
											objets[i].pixelSize(10)
										}
									}
								}
							}
							this.transformer()
							this.$socket.emit('reajouterpage', { tableau: this.tableau, page: donnees.nouveau, items: copieItems })
							if (donnees.nouveau === this.page) {
								this.afficherPage(this.page)
							}
						}.bind(this))
					}.bind(this))
				}.bind(this))
				break
			}
			if (erreur === true) {
				this.positionHistorique += 1
			}
			this.$nextTick(function () {
				this.items[this.page].forEach(function (item, index) {
					if (item.objet === 'ancre') {
						this.items[this.page][index].visible = false
					}
				}.bind(this))
				this.transformer()
				this.$forceUpdate()
			}.bind(this))
		},
		refaire () {
			if (this.positionHistorique === this.historique.length) {
				return false
			}
			const donnees = this.historique[this.positionHistorique]
			this.page = donnees.page
			const items = []
			const itemPrecedent = {}
			let objets = []
			let obj
			let erreur = false
			// const images = []
			this.reinitialiserSelection()
			switch (donnees.type) {
			case 'ajouter':
				donnees.nouveau.forEach(function (item) {
					donnees.items[this.page].forEach(function (itemP, indexP) {
						if (item.name === itemP.name) {
							items.push(indexP)
						}
					})
					if (item.hasOwnProperty('fichier') && item.fichier !== '') {
						this.imagesASupprimer.forEach(function (image, index) {
							if (image === item.fichier) {
								this.imagesASupprimer.splice(index, 1)
							}
						}.bind(this))
					}
				}.bind(this))
				if (donnees.nouveau[0].objet === 'image' && donnees.nouveau[0].fichier !== '') {
					const image = new window.Image()
					if (this.verifierURL(donnees.nouveau[0].fichier) === false) {
						image.src = '/fichiers/' + this.tableau + '/' + donnees.nouveau[0].fichier
					} else {
						image.crossOrigin = 'Anonymous'
						image.src = donnees.nouveau[0].fichier
					}
					image.onload = function () {
						donnees.nouveau[0].image = image
						this.items[this.page].splice(...[items[0], 0].concat(donnees.nouveau))
						this.$nextTick(function () {
							this.transformer()
							this.$forceUpdate()
							const copieItems = JSON.parse(JSON.stringify(donnees.nouveau))
							copieItems.forEach(function (item) {
								if (item.objet === 'image') {
									delete item.image
								}
							})
							this.$nextTick(function () {
								objets = this.$refs.transformer.getNode().getStage().find((item) => {
									return item.getAttrs().objet && item.getAttrs().objet === 'image'
								})
								for (let i = 0; i < objets.length; i++) {
									if (donnees.nouveau[0].name === objets[i].getAttrs().name) {
										if (donnees.nouveau[0].filtre === 'Aucun') {
											objets[i].filters([])
										} else {
											objets[i].cache()
											objets[i].filters([Konva.Filters[donnees.nouveau[0].filtre]])
											if (donnees.nouveau[0].filtre === 'Pixelate') {
												objets[i].pixelSize(10)
											}
										}
									}
								}
								this.transformer()
								this.$socket.emit('reajouter', { tableau: this.tableau, page: this.page, items: copieItems, index: items[0] })
							}.bind(this))
						}.bind(this))
					}.bind(this)
				} else if (donnees.nouveau[0].objet !== 'image') {
					donnees.nouveau.forEach(function (item) {
						if (item.objet === 'ancre') {
							item.visible = false
						}
					})
					this.items[this.page].splice(...[items[0], 0].concat(donnees.nouveau))
					this.$nextTick(function () {
						this.transformer()
						this.$forceUpdate()
						const copieItems = JSON.parse(JSON.stringify(donnees.nouveau))
						copieItems.forEach(function (item) {
							if (item.objet === 'image') {
								delete item.image
							}
						})
						this.$socket.emit('reajouter', { tableau: this.tableau, page: this.page, items: copieItems, index: items[0] })
					}.bind(this))
				}
				break
			case 'supprimer':
				this.items[this.page].forEach(function (item) {
					if (item.name === donnees.nom && item.name.substring(0, 4) === 'imag') {
						obj = this.items[this.page].find(r => r.name === item.name)
						if (obj) {
							this.imagesASupprimer.push(obj.fichier)
						} else {
							erreur = true
						}
					}
					if (item.name === donnees.nom && item.name.substring(0, 4) === 'ancr') {
						this.items[this.page] = this.items[this.page].filter(r => !r.name.includes(item.name.split('_')[1]))
					} else if (item.name === donnees.nom) {
						this.items[this.page] = this.items[this.page].filter(r => !r.name.includes(item.name))
					}
				}.bind(this))
				this.$forceUpdate()
				this.$socket.emit('supprimer', { tableau: this.tableau, page: this.page, nom: donnees.nom })
				break
			case 'verrouiller':
				donnees.nom.forEach(function (nom) {
					this.items[this.page].forEach(function (item) {
						if (item.name === nom) {
							item.draggable = false
							item.verrouille = true
						}
					})
				}.bind(this))
				this.$socket.emit('deverrouiller', { tableau: this.tableau, page: this.page, noms: donnees.nom })
				break
			case 'deverrouiller':
				donnees.nom.forEach(function (nom) {
					this.items[this.page].forEach(function (item) {
						if (item.name === nom) {
							item.draggable = true
							item.verrouille = false
						}
					})
				}.bind(this))
				this.$socket.emit('verrouiller', { tableau: this.tableau, page: this.page, noms: donnees.nom })
				break
			case 'modifierposition':
				this.items[this.page].forEach(function (item, index) {
					if (item.name === donnees.nom) {
						this.items[this.page].splice(index, 1)
						this.items[this.page].splice(donnees.nouveau, 0, item)
					}
				}.bind(this))
				this.$socket.emit('modifierposition', { tableau: this.tableau, page: this.page, nom: this.nom, index: donnees.nouveau })
				break
			case 'deplacer':
				donnees.nouveau.forEach(function (itemNouveau) {
					this.items[this.page].forEach(function (item) {
						if (item.name === itemNouveau.nom) {
							item.x = itemNouveau.x
							item.y = itemNouveau.y
							if (itemNouveau.hasOwnProperty('points')) {
								item.points = itemNouveau.points
							}
						}
					})
				}.bind(this))
				this.$socket.emit('deplacer', { tableau: this.tableau, page: this.page, items: donnees.nouveau })
				break
			case 'redimensionner':
				donnees.items[this.page].forEach(function (item) {
					if (item.name === donnees.nouveau.name) {
						itemPrecedent.name = item.name
						itemPrecedent.x = item.x
						itemPrecedent.y = item.y
						itemPrecedent.width = item.width
						itemPrecedent.height = item.height
						itemPrecedent.rotation = item.rotation
						itemPrecedent.scaleX = item.scaleX
						itemPrecedent.scaleY = item.scaleY
					}
				})
				if (itemPrecedent.hasOwnProperty('name') === true) {
					this.items[this.page].forEach(function (item) {
						if (item.name === donnees.nouveau.name) {
							item.x = itemPrecedent.x
							item.y = itemPrecedent.y
							item.width = itemPrecedent.width
							item.height = itemPrecedent.height
							item.rotation = itemPrecedent.rotation
							item.scaleX = itemPrecedent.scaleX
							item.scaleY = itemPrecedent.scaleY
						}
					})
					this.$socket.emit('redimensionner', { tableau: this.tableau, page: this.page, item: itemPrecedent })
				}
				break
			case 'modifiertransparence':
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					obj.opacity = donnees.nouveau
					this.$socket.emit('modifiertransparence', { tableau: this.tableau, page: this.page, nom: donnees.nom, transparence: donnees.nouveau })
				} else {
					erreur = true
				}
				break
			case 'modifiertexte':
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					if (donnees.nom.substring(0, 4) === 'text') {
						obj.text = donnees.nouveau
					} else {
						obj.text.text = donnees.nouveau
					}
					this.$socket.emit('modifiertexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, texte: donnees.nouveau })
				} else {
					erreur = true
				}
				break
			case 'modifiertailletexte':
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					obj.fontSize = donnees.nouveau
					this.$socket.emit('modifiertailletexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, taille: donnees.nouveau })
				} else {
					erreur = true
				}
				break
			case 'modifieralignementtexte':
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					obj.align = donnees.nouveau
					obj.opacity = 0.9
					obj.opacity = 1
					this.$socket.emit('modifieralignementtexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, alignement: donnees.nouveau })
				} else {
					erreur = true
				}
				break
			case 'modifierstyletexte':
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					if (obj.objet === 'label') {
						obj = obj.text
					}
					obj.fontStyle = donnees.nouveau
					this.$socket.emit('modifierstyletexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, style: donnees.nouveau })
				} else {
					erreur = true
				}
				break
			case 'modifierdecorationtexte':
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					if (obj.objet === 'label') {
						obj = obj.text
					}
					obj.textDecoration = donnees.nouveau
					this.$socket.emit('modifierdecorationtexte', { tableau: this.tableau, page: this.page, nom: donnees.nom, decoration: donnees.nouveau })
				} else {
					erreur = true
				}
				break
			case 'modifierfiltre':
				objets = this.$refs.transformer.getNode().getStage().find((item) => {
					return item.getAttrs().objet && item.getAttrs().objet === 'image'
				})
				for (let i = 0; i < objets.length; i++) {
					if (donnees.nom === objets[i].getAttrs().name) {
						if (donnees.nouveau === 'Aucun') {
							objets[i].filters([])
						} else {
							objets[i].cache()
							objets[i].filters([Konva.Filters[donnees.nouveau]])
							if (donnees.nouveau === 'Pixelate') {
								objets[i].pixelSize(10)
							}
						}
					}
				}
				this.$socket.emit('modifierfiltre', { tableau: this.tableau, page: this.page, nom: donnees.nom, filtre: donnees.nouveau })
				break
			case 'modifiercouleur':
				obj = this.items[this.page].find(r => r.name === donnees.nom)
				if (obj) {
					if (obj.objet === 'rectangle' || obj.objet === 'cercle' || obj.objet === 'dessin') {
						obj.stroke = donnees.nouveau
					} else if (obj.objet === 'rectangle-plein' || obj.objet === 'cercle-plein' || obj.objet === 'etoile' || obj.objet === 'surlignage' || obj.objet === 'texte') {
						obj.fill = donnees.nouveau
					} else if (obj.objet === 'ligne' || obj.objet === 'fleche' || obj.objet === 'ancre') {
						obj.stroke = donnees.nouveau
						obj.fill = donnees.nouveau
					} else if (obj.objet === 'label' || obj.objet === 'commentaire') {
						obj.tag.fill = donnees.nouveau
					}
					this.$socket.emit('modifiercouleur', { tableau: this.tableau, page: this.page, nom: donnees.nom, couleur: donnees.nouveau })
				} else {
					erreur = true
				}
				break
			case 'ajouterpage':
				this.items.splice(donnees.nouveau, 1, [])
				this.$socket.emit('ajouterpage', { tableau: this.tableau, page: donnees.nouveau })
				this.page = donnees.nouveau
				this.afficherPage(this.page)
				break
			case 'supprimerpage':
				this.items.splice(donnees.nouveau, 1)
				this.$socket.emit('supprimerpage', { tableau: this.tableau, page: donnees.nouveau })
				if (this.page === donnees.nouveau && this.page > 0) {
					this.afficherPage(this.page)
				} else if (this.page === donnees.nouveau && this.page === 0) {
					this.page = 0
					this.afficherPage(this.page)
				}
				break
			}
			if (erreur === false) {
				this.positionHistorique += 1
				this.$nextTick(function () {
					this.items[this.page].forEach(function (item, index) {
						if (item.objet === 'ancre') {
							this.items[this.page][index].visible = false
						}
					}.bind(this))
					this.transformer()
					this.$forceUpdate()
				}.bind(this))
			}
		},
		activerDeplacement () {
			this.items[this.page].forEach(function (item) {
				item.verrouille ? item.draggable = false : item.draggable = true
			})
		},
		desactiverDeplacement () {
			this.items[this.page].forEach(function (item) {
				item.draggable = false
			})
		},
		activerSelecteur () {
			this.$nextTick(function () {
				if (document.querySelector('#couleur-selecteur')) {
					document.querySelector('#couleur-selecteur').addEventListener('change', this.modifierCouleurSelecteur)
				}
			}.bind(this))
		},
		desactiverSelecteur () {
			this.$nextTick(function () {
				if (document.querySelector('#couleur-selecteur')) {
					document.querySelector('#couleur-selecteur').removeEventListener('change', this.modifierCouleurSelecteur)
				}
			}.bind(this))
		},
		afficherCommentaires () {
			if (this.menu !== 'commentaires') {
				this.elementPrecedent = (document.activeElement || document.body)
				this.menu = 'commentaires'
				setTimeout(function () {
					document.querySelector('#menu-commentaires').classList.add('ouvert')
					document.querySelector('#menu-commentaires .accordeon .statut').focus()
				}, 0)
			} else if (this.menu === 'commentaires') {
				this.menu = ''
			}
		},
		afficherModaleCommentaires (itemId) {
			this.afficherItemCommentaire(itemId)
			this.gererCommentaires()
		},
		gererMenuUtilisateurs () {
			if (this.menu !== 'utilisateurs') {
				this.elementPrecedent = (document.activeElement || document.body)
				this.menu = 'utilisateurs'
				setTimeout(function () {
					document.querySelector('#menu-utilisateurs').classList.add('ouvert')
					document.querySelector('#menu-utilisateurs .moi .icone').focus()
				}, 0)
			} else if (this.menu === 'utilisateurs') {
				this.menu = ''
			}
		},
		definirUtilisateurs (donnees) {
			let utilisateurs = []
			const autresUtilisateurs = []
			donnees.forEach(function (utilisateur) {
				if (utilisateur.identifiant === this.identifiant && utilisateurs.length === 0) {
					utilisateurs.push(utilisateur)
				} else if (utilisateur.identifiant !== this.identifiant) {
					autresUtilisateurs.push(utilisateur)
				}
			}.bind(this))
			utilisateurs = utilisateurs.concat(autresUtilisateurs)
			this.utilisateurs = utilisateurs
		},
		afficherModifierNom () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'modifier-nom'
			this.nouveaunom = this.nomUtilisateur
			this.$nextTick(function () {
				document.querySelector('.modale input').focus()
			})
		},
		modifierNom () {
			if (this.nouveaunom !== '') {
				this.chargement = true
				axios.post(this.hote + '/api/modifier-nom', {
					nom: this.nouveaunom
				}).then(function (reponse) {
					this.chargement = false
					const donnees = reponse.data
					if (donnees === 'erreur') {
						this.fermerModaleModifierNom()
						this.message = this.$t('erreurCommunicationServeur')
					} else if (donnees === 'non_autorise') {
						this.fermerModaleModifierNom()
						this.message = this.$t('actionNonAutorisee')
					} else if (donnees === 'nom_modifie') {
						this.nomUtilisateur = this.nouveaunom
						this.utilisateurs.forEach(function (utilisateur) {
							if (utilisateur.identifiant === this.identifiant) {
								utilisateur.nom = this.nouveaunom
							}
						}.bind(this))
						this.notification = this.$t('nomModifie')
						this.fermerModaleModifierNom()
					}
				}.bind(this)).catch(function () {
					this.chargement = false
					this.fermerModaleModifierNom()
					this.message = this.$t('erreurCommunicationServeur')
				}.bind(this))
			} else if (this.nouveaunom === '') {
				this.message = this.$t('remplirNom')
			}
		},
		fermerModaleModifierNom () {
			this.modale = ''
			this.nouveaunom = ''
			this.gererFocus()
		},
		afficherModaleModifierRoleUtilisateur (identifiant, role) {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'modifier-role-utilisateur'
			let message = ''
			if (role === 'editeur') {
				message = this.$t('confirmationAttribuerDroitsUtilisateur')
			} else {
				message = this.$t('confirmationRetirerDroitsUtilisateur')
			}
			this.donneesRoleUtilisateur = { identifiant: identifiant, role: role, message: message }
			this.$nextTick(function () {
				document.querySelector('.modale .bouton').focus()
			})
		},
		modifierRoleUtilisateur () {
			this.chargement = true
			this.$socket.emit('modifierroleutilisateur', { tableau: this.tableau, admin: this.identifiant, identifiant: this.donneesRoleUtilisateur.identifiant, role: this.donneesRoleUtilisateur.role })
			this.modale = ''
			this.gererFocus()
			this.donneesRoleUtilisateur = { identifiant: '', role: '', message: '' }
		},
		modifierTitre () {
			const titre = document.querySelector('#titre-tableau').value
			if (titre !== '') {
				this.chargement = true
				axios.post(this.hote + '/api/modifier-titre', {
					tableau: this.tableau,
					nouveautitre: titre
				}).then(function (reponse) {
					this.chargement = false
					const donnees = reponse.data
					if (donnees === 'erreur') {
						this.message = this.$t('erreurCommunicationServeur')
					} else if (donnees === 'non_autorise') {
						this.message = this.$t('actionNonAutorisee')
					} else if (donnees === 'titre_modifie') {
						this.titre = titre
						document.title = titre + ' - Digiboard by La Digitale'
						this.notification = this.$t('titreModifie')
					}
				}.bind(this)).catch(function () {
					this.chargement = false
					this.message = this.$t('erreurCommunicationServeur')
				}.bind(this))
			} else if (titre === '') {
				this.message = this.$t('remplirNouveauTitre')
			}
		},
		modifierLangue (langue) {
			if (this.langue !== langue) {
				axios.post(this.hote + '/api/modifier-langue', {
					langue: langue
				}).then(function () {
					this.$i18n.locale = langue
					document.getElementsByTagName('html')[0].setAttribute('lang', langue)
					this.langue = langue
					this.notification = this.$t('langueModifiee')
				}.bind(this)).catch(function () {
					this.message = this.$t('erreurCommunicationServeur')
				}.bind(this))
			}
		},
		modifierFond (fond) {
			if (fond.substring(0, 8) === 'couleur-') {
				this.modifierCouleurFond('#' + fond.substring(8).replace('.png', ''))
			} else {
				this.chargement = true
				axios.post(this.hote + '/api/modifier-fond', {
					tableau: this.tableau,
					fond: fond
				}).then(function (reponse) {
					this.chargement = false
					const donnees = reponse.data
					if (donnees === 'erreur') {
						this.message = this.$t('erreurCommunicationServeur')
					} else if (donnees === 'non_autorise') {
						this.message = this.$t('actionNonAutorisee')
					} else if (donnees === 'fond_modifie') {
						this.options.fond = fond
						document.querySelector('#tableau .konvajs-content').style.backgroundColor = 'transparent'
						document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'url(/img/' + fond + ')'
						document.querySelector('#tableau .konvajs-content').style.backgroundRepeat = 'repeat'
						this.notification = this.$t('fondModifie')
						this.$socket.emit('modifierfond', { tableau: this.tableau, fond: fond })
					}
				}.bind(this)).catch(function () {
					this.chargement = false
					this.message = this.$t('erreurCommunicationServeur')
				}.bind(this))
			}
		},
		modifierCouleurFond (couleur) {
			this.chargement = true
			axios.post(this.hote + '/api/modifier-fond', {
				tableau: this.tableau,
				fond: couleur
			}).then(function (reponse) {
				this.chargement = false
				const donnees = reponse.data
				if (donnees === 'erreur') {
					this.message = this.$t('erreurCommunicationServeur')
				} else if (donnees === 'non_autorise') {
					this.message = this.$t('actionNonAutorisee')
				} else if (donnees === 'fond_modifie') {
					this.options.fond = couleur
					document.querySelector('#tableau .konvajs-content').style.backgroundColor = couleur
					document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'none'
					this.notification = this.$t('fondModifie')
					this.$socket.emit('modifierfond', { tableau: this.tableau, fond: couleur })
				}
			}.bind(this)).catch(function () {
				this.chargement = false
				this.message = this.$t('erreurCommunicationServeur')
			}.bind(this))
		},
		activerInput (id) {
			document.querySelector('#' + id).click()
		},
		modifierMultiPage (event) {
			if (event.target.checked === true) {
				this.$socket.emit('modifiermultipage', { tableau: this.tableau, multipage: 'oui' })
			} else {
				this.$socket.emit('modifiermultipage', { tableau: this.tableau, multipage: 'non' })
			}
			this.chargement = true
		},
		modifierEdition (event) {
			if (event.target.checked === true) {
				this.$socket.emit('modifieredition', { tableau: this.tableau, edition: 'ouverte' })
			} else {
				this.$socket.emit('modifieredition', { tableau: this.tableau, edition: 'fermee' })
			}
			this.chargement = true
		},
		modifierAffichageAuteur (event) {
			if (event.target.checked === true) {
				this.$socket.emit('modifieraffichageauteur', { tableau: this.tableau, affichageAuteur: 'oui' })
			} else {
				this.$socket.emit('modifieraffichageauteur', { tableau: this.tableau, affichageAuteur: 'non' })
			}
			this.chargement = true
		},
		afficherModaleAcces () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'modifier-acces'
			this.$nextTick(function () {
				document.querySelector('.modale select').focus()
			})
		},
		modifierAcces () {
			if (this.question !== '' && this.reponse !== '' && this.nouvellequestion !== '' && this.nouvellereponse !== '') {
				this.chargement = true
				axios.post(this.hote + '/api/modifier-acces', {
					tableau: this.tableau,
					question: this.question,
					reponse: this.reponse,
					nouvellequestion: this.nouvellequestion,
					nouvellereponse: this.nouvellereponse
				}).then(function (reponse) {
					this.chargement = false
					const donnees = reponse.data
					if (donnees === 'erreur') {
						this.fermerModaleAcces()
						this.message = this.$t('erreurCommunicationServeur')
					} else if (donnees === 'non_autorise') {
						this.fermerModaleAcces()
						this.message = this.$t('actionNonAutorisee')
					} else if (donnees === 'informations_incorrectes') {
						this.fermerModaleAcces()
						this.message = this.$t('informationsIncorrectes')
					} else if (donnees === 'acces_modifie') {
						this.notification = this.$t('accesModifie')
						this.fermerModaleAcces()
					}
				}.bind(this)).catch(function () {
					this.chargement = false
					this.fermerModaleAcces()
					this.message = this.$t('erreurCommunicationServeur')
				}.bind(this))
			} else if (this.question === '') {
				this.message = this.$t('selectionnerQuestionSecreteActuelle')
			} else if (this.reponse === '') {
				this.message = this.$t('indiquerReponseSecreteActuelle')
			} else if (this.nouvellequestion === '') {
				this.message = this.$t('selectionnerNouvelleQuestionSecrete')
			} else if (this.nouvellereponse === '') {
				this.message = this.$t('indiquerNouvelleReponseSecrete')
			}
		},
		fermerModaleAcces () {
			this.modale = ''
			this.question = ''
			this.reponse = ''
			this.nouvellequestion = ''
			this.nouvellereponse = ''
			this.gererFocus()
		},
		terminerSession () {
			this.chargement = true
			axios.post(this.hote + '/api/terminer-session').then(function (reponse) {
				this.chargement = false
				const donnees = reponse.data
				if (donnees === 'session_terminee') {
					this.modale = ''
					this.menu = ''
					this.admin = false
					this.notification = this.$t('sessionTerminee')
				}
			}.bind(this)).catch(function () {
				this.chargement = false
				this.modale = ''
				this.menu = ''
				this.message = this.$t('erreurCommunicationServeur')
			}.bind(this))
		},
		afficherModaleDebloquer () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'debloquer-tableau'
			this.$nextTick(function () {
				document.querySelector('.modale select').focus()
			})
		},
		debloquerTableau () {
			if (this.question !== '' && this.reponse !== '') {
				this.chargement = true
				axios.post(this.hote + '/api/debloquer-tableau', {
					tableau: this.tableau,
					question: this.question,
					reponse: this.reponse,
					type: 'utilisateur'
				}).then(function (reponse) {
					this.chargement = false
					const donnees = reponse.data
					if (donnees === 'erreur') {
						this.fermerModaleDebloquer()
						this.message = this.$t('erreurCommunicationServeur')
					} else if (donnees === 'informations_incorrectes') {
						this.fermerModaleDebloquer()
						this.message = this.$t('informationsIncorrectes')
					} else if (donnees === 'tableau_debloque') {
						this.admin = true
						this.role = 'auteur'
						this.notification = this.$t('connexionReussie')
						this.fermerModaleDebloquer()
					}
				}.bind(this)).catch(function () {
					this.chargement = false
					this.fermerModaleDebloquer()
					this.message = this.$t('erreurCommunicationServeur')
				}.bind(this))
			} else if (this.question === '') {
				this.message = this.$t('selectionnerQuestionSecrete')
			} else if (this.reponse === '') {
				this.message = this.$t('remplirReponseSecrete')
			}
		},
		fermerModaleDebloquer () {
			this.modale = ''
			this.question = ''
			this.reponse = ''
			this.gererFocus()
		},
		afficherModaleSupprimer () {
			this.elementPrecedent = (document.activeElement || document.body)
			this.modale = 'supprimer-tableau'
			this.$nextTick(function () {
				document.querySelector('.modale .bouton').focus()
			})
		},
		supprimerTableau () {
			this.modale = ''
			this.chargement = true
			axios.post(this.hote + '/api/supprimer-tableau', {
				tableau: this.tableau
			}).then(function (reponse) {
				const donnees = reponse.data
				if (donnees === 'erreur') {
                    this.chargement = false
					this.modale = ''
					this.message = this.$t('erreurCommunicationServeur')
				} else if (donnees === 'non_autorise') {
                    this.chargement = false
					this.modale = ''
					this.message = this.$t('actionNonAutorisee')
				} else if (donnees === 'tableau_supprime') {
					window.location.href = '/'
				}
			}.bind(this)).catch(function () {
				this.chargement = false
				this.modale = ''
				this.message = this.$t('erreurCommunicationServeur')
			}.bind(this))
		},
		gererClavier (event) {
			if (event.key === 'Escape' && this.message !== '') {
				this.message = ''
			} else if (event.key === 'Escape' && this.modale === 'texte') {
				this.fermerModaleTexte()
			} else if (event.key === 'Escape' && this.modale === 'commentaire') {
				this.fermerModaleCommentaire()
			} else if (event.key === 'Escape' && this.modale === 'recherche') {
				this.fermerModaleRecherche()
			} else if (event.key === 'Escape' && this.modale === 'debloquer-tableau') {
				this.fermerModaleDebloquer()
			} else if (event.key === 'Escape' && this.modale === 'modifier-nom') {
				this.fermerModaleModifierNom()
			} else if (event.key === 'Escape' && this.modale === 'modifier-acces') {
				this.fermerModaleAcces()
			} else if (event.key === 'Escape' && this.modale !== '') {
				this.modale = ''
				this.gererFocus()
			} else if (event.key === 'Escape' && this.menu !== '') {
				this.menu = ''
				this.gererFocus()
			}
		},
		gererFocus () {
			if (this.elementPrecedent) {
				this.elementPrecedent.focus()
				this.elementPrecedent = null
			}
		},
		quitterPage () {
			this.$socket.emit('supprimerimages', { tableau: this.tableau, images: this.imagesASupprimer })
			this.$socket.emit('deconnexion', this.tableau)
		},
		verifierURL (lien) {
			let url
			try {
				url = new URL(lien)
			} catch (_) {
				return false
			}
			return url.protocol === 'http:' || url.protocol === 'https:'
		},
		ecouterSocket () {
			this.$socket.on('connexion', function (utilisateurs) {
				this.definirUtilisateurs(utilisateurs)
			}.bind(this))

			this.$socket.on('deconnexion', function (identifiant) {
				const utilisateurs = this.utilisateurs
				utilisateurs.forEach(function (utilisateur, index) {
					if (utilisateur.identifiant === identifiant) {
						utilisateurs.splice(index, 1)
					}
				})
				this.utilisateurs = utilisateurs
			}.bind(this))

			this.$socket.on('erreur', function () {
				this.message = this.$t('erreurCommunicationServeur')
			}.bind(this))

			this.$socket.on('ajouter', function (donnees) {
				if (donnees.page === this.page) {
					if (donnees.items[0].objet === 'image' && donnees.items[0].fichier !== '') {
						const image = new window.Image()
						if (this.verifierURL(donnees.items[0].fichier) === false) {
							image.src = '/fichiers/' + this.tableau + '/' + donnees.items[0].fichier
						} else {
							image.crossOrigin = 'Anonymous'
							image.src = donnees.items[0].fichier
						}
						image.onload = function () {
							donnees.items[0].image = image
							this.items[donnees.page].push(...donnees.items)
							this.$nextTick(function () {
								const transformer = this.$refs.transformer.getNode()
								const stage = transformer.getStage()
								const objets = stage.find((item) => {
									return item.getAttrs().objet && item.getAttrs().objet === 'image'
								})
								for (let i = 0; i < objets.length; i++) {
									if (donnees.items[0].name === objets[i].getAttrs().name) {
										if (donnees.items[0].filtre === 'Aucun') {
											objets[i].filters([])
										} else {
											objets[i].cache()
											objets[i].filters([Konva.Filters[donnees.items[0].filtre]])
											if (donnees.items[0].filtre === 'Pixelate') {
												objets[i].pixelSize(10)
											}
										}
									}
								}
								this.$nextTick(function () {
									this.transformer()
								}.bind(this))
							}.bind(this))
						}.bind(this)
					} else if (donnees.items[0].objet !== 'image') {
						donnees.items.forEach(function (item) {
							if (item.objet === 'ancre') {
								item.visible = false
							}
						})
						this.items[donnees.page].push(...donnees.items)
						this.$nextTick(function () {
							this.transformer()
						}.bind(this))
					}
				} else {
					this.items[donnees.page].push(...donnees.items)
					this.$nextTick(function () {
						this.transformer()
					}.bind(this))
				}
			}.bind(this))

			this.$socket.on('reajouter', function (donnees) {
				if (donnees.page === this.page) {
					if (donnees.items[0].objet === 'image' && donnees.items[0].fichier !== '') {
						const image = new window.Image()
						if (this.verifierURL(donnees.items[0].fichier) === false) {
							image.src = '/fichiers/' + this.tableau + '/' + donnees.items[0].fichier
						} else {
							image.crossOrigin = 'Anonymous'
							image.src = donnees.items[0].fichier
						}
						image.onload = function () {
							donnees.items[0].image = image
							this.items[donnees.page].splice(...[donnees.index, 0].concat(donnees.items))
							this.$nextTick(function () {
								this.transformer()
							}.bind(this))
						}.bind(this)
					} else if (donnees.items[0].objet !== 'image') {
						donnees.items.forEach(function (item) {
							if (item.objet === 'ancre') {
								item.visible = false
							}
						})
						this.items[donnees.page].splice(...[donnees.index, 0].concat(donnees.items))
						this.$nextTick(function () {
							this.transformer()
						}.bind(this))
					}
				} else {
					this.items[donnees.page].splice(...[donnees.index, 0].concat(donnees.items))
					this.$nextTick(function () {
						this.transformer()
					}.bind(this))
				}
			}.bind(this))

			this.$socket.on('verrouiller', function (donnees) {
				for (let i = 0; i < donnees.noms.length; i++) {
					const item = this.items[this.page].find(r => r.name === donnees.noms[i])
					if (item) {
						item.draggable = false
						item.verrouille = true
					}
				}
				if (donnees.noms.includes(this.nom)) {
					this.reinitialiserSelection()
				}
				if (donnees.noms.length === 1) {
					this.transformer()
				} else {
					this.$refs.transformer.getNode().getLayer().batchDraw()
				}
			}.bind(this))

			this.$socket.on('deverrouiller', function (donnees) {
				for (let i = 0; i < donnees.noms.length; i++) {
					const item = this.items[this.page].find(r => r.name === donnees.noms[i])
					if (item && item.objet !== 'dessin') {
						item.draggable = true
						item.verrouille = false
					}
				}
				if (donnees.noms.length === 1) {
					this.transformer()
				} else {
					this.$refs.transformer.getNode().getLayer().batchDraw()
				}
			}.bind(this))

			this.$socket.on('modifierposition', function (donnees) {
				this.items[donnees.page].forEach(function (item, index) {
					if (item.name === donnees.nom) {
						this.items[donnees.page].splice(index, 1)
						this.items[donnees.page].splice(donnees.index, 0, item)
					}
				}.bind(this))
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('deplacer', function (donnees) {
				for (let i = 0; i < donnees.items.length; i++) {
					const item = this.items[this.page].find(r => r.name === donnees.items[i].nom)
					if (item) {
						item.x = donnees.items[i].x
						item.y = donnees.items[i].y
						if (donnees.items[i].hasOwnProperty('points')) {
							item.points = donnees.items[i].points
						}
					}
				}
				if (donnees.items.length === 1) {
					this.transformer()
				} else {
					this.$refs.transformer.getNode().getLayer().batchDraw()
				}
			}.bind(this))

			this.$socket.on('redimensionner', function (donnees) {
				this.items[donnees.page].forEach(function (item) {
					if (item.name === donnees.item.name) {
						item.x = donnees.item.x
						item.y = donnees.item.y
						item.width = donnees.item.width
						item.height = donnees.item.height
						item.rotation = donnees.item.rotation
						item.scaleX = donnees.item.scaleX
						item.scaleY = donnees.item.scaleY
					}
				})
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('modifiertransparence', function (donnees) {
				const item = this.items[donnees.page].find(r => r.name === donnees.nom)
				item.opacity = donnees.transparence
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('modifiertexte', function (donnees) {
				const type = donnees.nom.substring(0, 4)
				const item = this.items[donnees.page].find(r => r.name === donnees.nom)
				const texte = donnees.texte
				if (type === 'text') {
					item.text = texte
				} else {
					item.text.text = texte
				}
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('modifiertailletexte', function (donnees) {
				const item = this.items[donnees.page].find(r => r.name === donnees.nom)
				item.fontSize = donnees.taille
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('modifieralignementtexte', function (donnees) {
				const item = this.items[donnees.page].find(r => r.name === donnees.nom)
				item.align = donnees.alignement
				item.opacity = 0.9
				item.opacity = 1
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('modifierstyletexte', function (donnees) {
				const item = this.items[donnees.page].find(r => r.name === donnees.nom)
				item.fontStyle = donnees.style
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('modifierdecorationtexte', function (donnees) {
				const item = this.items[donnees.page].find(r => r.name === donnees.nom)
				item.textDecoration = donnees.decoration
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('modifierfiltre', function (donnees) {
				const transformer = this.$refs.transformer.getNode()
				const stage = transformer.getStage()
				const objets = stage.find((item) => {
					return item.getAttrs().objet && item.getAttrs().objet === 'image'
				})
				for (let i = 0; i < objets.length; i++) {
					if (donnees.nom === objets[i].getAttrs().name) {
						if (donnees.filtre === 'Aucun') {
							objets[i].filters([])
						} else {
							objets[i].cache()
							objets[i].filters([Konva.Filters[donnees.filtre]])
							if (donnees.filtre === 'Pixelate') {
								objets[i].pixelSize(10)
							}
						}
					}
				}
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('modifiercouleur', function (donnees) {
				const item = this.items[donnees.page].find(r => r.name === donnees.nom)
				if (item.objet === 'rectangle' || item.objet === 'cercle' || item.objet === 'dessin') {
					item.stroke = donnees.couleur
				} else if (item.objet === 'rectangle-plein' || item.objet === 'cercle-plein' || item.objet === 'etoile' || item.objet === 'surlignage' || item.objet === 'texte') {
					item.fill = donnees.couleur
				} else if (item.objet === 'ligne' || item.objet === 'fleche' || item.objet === 'ancre') {
					item.stroke = donnees.couleur
					item.fill = donnees.couleur
				} else if (item.objet === 'label') {
					item.tag.fill = donnees.couleur
				}
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('importer', function (donnees) {
				this.reinitialiserSelection()
				this.page = 0
				this.historique = []
				this.positionHistorique = 0
				this.positionActuelleHistorique = 0
				this.donnees = donnees.items
				this.commentaires = donnees.commentaires
				this.options.fond = donnees.fond
				if (donnees.fond.substring(0, 1) === '#') {
					document.querySelector('#tableau .konvajs-content').style.backgroundColor = donnees.fond
					document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'none'
				} else {
					document.querySelector('#tableau .konvajs-content').style.backgroundColor = 'transparent'
					document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'url(/img/' + donnees.fond + ')'
					document.querySelector('#tableau .konvajs-content').style.backgroundRepeat = 'repeat'
				}
				this.reinitialiserImages()
				this.chargerItems(donnees.items, this.page)
			}.bind(this))

			this.$socket.on('supprimer', function (donnees) {
				const type = donnees.nom.substring(0, 4)
				let items
				if (type === 'ancr') {
					items = this.items[donnees.page].filter(r => !r.name.includes(donnees.nom.split('_')[1]))
				} else {
					items = this.items[donnees.page].filter(r => !r.name.includes(donnees.nom))
				}
				this.items.splice(donnees.page, 1, items)
				if (this.nom === donnees.nom) {
					this.nom = ''
					this.objet = ''
				}
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('supprimerselection', function (donnees) {
				donnees.noms.forEach(function (nom) {
					const type = nom.substring(0, 4)
					let items
					if (type === 'ancr') {
						items = this.items[donnees.page].filter(r => !r.name.includes(nom.split('_')[1]))
					} else {
						items = this.items[donnees.page].filter(r => !r.name.includes(nom))
					}
					this.items.splice(donnees.page, 1, items)
					if (this.nom === nom) {
						this.nom = ''
						this.objet = ''
					}
				}.bind(this))
				this.$nextTick(function () {
					this.transformer()
				}.bind(this))
			}.bind(this))

			this.$socket.on('ajouterpage', function () {
				this.items.push([])
			}.bind(this))

			this.$socket.on('reajouterpage', function (donnees) {
				const images = []
				for (let i = 0; i < donnees.items.length; i++) {
					if (donnees.items[i].objet === 'image' && donnees.items[i].fichier !== '') {
						const donneesImage = new Promise(function (resolve) {
							const image = new window.Image()
							if (this.verifierURL(donnees.items[i].fichier) === false) {
								image.src = '/fichiers/' + this.tableau + '/' + donnees.items[i].fichier
							} else {
								image.crossOrigin = 'Anonymous'
								image.src = donnees.items[i].fichier
							}
							image.onload = function () {
								donnees.items[i].image = image
								resolve('termine')
							}
							image.onerror = function () {
								resolve()
							}
						}.bind(this))
						images.push(donneesImage)
					} else if (donnees.items[i].objet !== 'image') {
						images.push('termine')
					}
				}
				Promise.all(images).then(function () {
					donnees.items.forEach(function (item) {
						if (item.objet === 'ancre') {
							item.visible = false
						}
					})
					this.items.splice(donnees.page, 0, donnees.items)
					this.$nextTick(function () {
						this.transformer()
						if (donnees.page === this.page) {
							this.afficherPage(this.page)
						}
					}.bind(this))
				}.bind(this))
			}.bind(this))

			this.$socket.on('deplacerpage', function (donnees) {
				const donneesPage = this.items.splice(donnees.ancienIndex, 1)[0]
				this.items.splice(donnees.nouvelIndex, 0, donneesPage)
			}.bind(this))

			this.$socket.on('supprimerpage', function (donnees) {
				this.items.splice(donnees.page, 1)
				if (this.page === donnees.page && this.page > 0) {
					this.page--
					this.afficherPage(this.page)
				} else if (this.page === donnees.page && this.page === 0) {
					this.page = 0
					this.afficherPage(this.page)
				}
			}.bind(this))

			this.$socket.on('enregistrercommentaire', function (donnees) {
				if (!this.commentaires.hasOwnProperty(donnees.itemId)) {
					this.commentaires[donnees.itemId] = []
				}
				this.commentaires[donnees.itemId].push(donnees.commentaire)
				if (this.page === donnees.page) {
					if (!this.commentairesPage.hasOwnProperty(donnees.itemId)) {
						this.commentairesPage[donnees.itemId] = []
					}
					this.commentairesPage[donnees.itemId].push(donnees.commentaire)
				}
				if (this.modale === 'commentaire' && this.itemId === donnees.itemId) {
					this.commentairesItem.push(donnees.commentaire)
				}
				if (donnees.commentaire.identifiant === this.identifiant) {
					this.editeur.content.innerHTML = ''
					this.commentaire = ''
					this.emojis = ''
				}
			}.bind(this))

			this.$socket.on('enregistrercommentairemodifie', function (donnees) {
				this.commentaires[donnees.itemId].forEach(function (commentaire) {
					if (commentaire.id === donnees.commentaire.id) {
						commentaire.texte = donnees.commentaire.texte
					}
				})
				if (this.commentairesPage.hasOwnProperty(donnees.itemId) === true) {
					this.commentairesPage[donnees.itemId].forEach(function (commentaire) {
						if (commentaire.id === donnees.commentaire.id) {
							commentaire.texte = donnees.commentaire.texte
						}
					})
				}
				if (this.modale === 'commentaire' && this.itemId === donnees.itemId) {
					this.commentairesItem.forEach(function (commentaire) {
						if (commentaire.id === donnees.commentaire.id) {
							commentaire.texte = donnees.commentaire.texte
						}
					})
				}
				if (donnees.commentaire.identifiant === this.identifiant) {
					this.annulerModifierCommentaire()
				}
			}.bind(this))

			this.$socket.on('supprimercommentaire', function (donnees) {
				this.commentaires[donnees.itemId].forEach(function (commentaire, index) {
					if (commentaire.id === donnees.commentaireId) {
						this.commentaires[donnees.itemId].splice(index, 1)
					}
				}.bind(this))
				if (this.commentairesPage.hasOwnProperty(donnees.itemId) === true) {
					this.commentairesPage[donnees.itemId].forEach(function (commentaire, index) {
						if (commentaire.id === donnees.commentaireId) {
							this.commentairesPage[donnees.itemId].splice(index, 1)
						}
					}.bind(this))
				}
				if (this.modale === 'commentaire' && this.itemId === donnees.itemId) {
					this.commentairesItem.forEach(function (commentaire, index) {
						if (commentaire.id === donnees.commentaireId) {
							this.commentairesItem.splice(index, 1)
						}
					}.bind(this))
				}
			}.bind(this))

			this.$socket.on('supprimercommentaires', function (donnees) {
				if (this.commentaires.hasOwnProperty(donnees.itemId) === true) {
					delete this.commentaires[donnees.itemId]
				}
				if (this.commentairesPage.hasOwnProperty(donnees.itemId) === true) {
					delete this.commentairesPage[donnees.itemId]
				}
				if (this.modale === 'commentaire' && this.itemId === donnees.itemId) {
					this.fermerModaleCommentaire()
				}
				if (this.menu === 'commentaires' && this.page === donnees.page && Object.keys(this.commentairesPage).length === 0) {
					this.menu = ''
				}
			}.bind(this))

			this.$socket.on('modifierfond', function (donnees) {
				this.options.fond = donnees.fond
				if (donnees.fond.substring(0, 1) === '#') {
					document.querySelector('#tableau .konvajs-content').style.backgroundColor = donnees.fond
					document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'none'
				} else {
					document.querySelector('#tableau .konvajs-content').style.backgroundColor = 'transparent'
					document.querySelector('#tableau .konvajs-content').style.backgroundImage = 'url(/img/' + donnees.fond + ')'
					document.querySelector('#tableau .konvajs-content').style.backgroundRepeat = 'repeat'
				}
			}.bind(this))

			this.$socket.on('modifieredition', function (edition) {
				this.options.edition = edition
				this.chargement = false
				if (!this.admin && edition === 'ouverte') {
					this.activerDeplacement()
					window.addEventListener('keydown', this.gererClavierAdmin, false)
					this.reinitialiserSelection()
					this.reinitialiserOutils()
				} else if (!this.admin && edition === 'fermee') {
					this.desactiverDeplacement()
					window.removeEventListener('keydown', this.gererClavierAdmin, false)
					this.reinitialiserSelection()
					this.reinitialiserOutils()
					this.$socket.emit('attribuerroleparticipant', { identifiant: this.identifiant })
				}
				if (this.admin && edition === 'ouverte') {
					this.notification = this.$t('editionAutorisee')
				} else if (this.admin && edition === 'fermee') {
					this.utilisateurs.forEach(function (utilisateur) {
						utilisateur.role = 'participant'
					})
					this.notification = this.$t('editionBloquee')
				}
			}.bind(this))

			this.$socket.on('modifiermultipage', function (multipage) {
				this.chargement = false
				this.options.multipage = multipage
				this.$nextTick()
				if (multipage === 'non') {
					this.page = 0
					this.afficherPage(this.page)
				}
				if (this.admin && multipage === 'oui') {
					this.notification = this.$t('modeMultiPageActive')
				} else if (this.admin && multipage === 'non') {
					this.notification = this.$t('modeMultiPageDesactive')
				}
			}.bind(this))

			this.$socket.on('modifieraffichageauteur', function (affichageAuteur) {
				this.chargement = false
				this.options.affichageAuteur = affichageAuteur
				if (this.admin && affichageAuteur === 'oui') {
					this.notification = this.$t('affichageAuteurActive')
				} else if (this.admin && affichageAuteur === 'non') {
					this.notification = this.$t('affichageAuteurDesactive')
				}
			}.bind(this))

			this.$socket.on('modifierroleutilisateur', function (donnees) {
				if (donnees.identifiant === this.identifiant) {
					this.role = donnees.role
					this.utilisateurs.forEach(function (utilisateur) {
						if (utilisateur.identifiant === this.identifiant) {
							utilisateur.role = donnees.role
						}
					}.bind(this))
					if (donnees.role === 'editeur') {
						this.activerDeplacement()
						window.addEventListener('keydown', this.gererClavierAdmin, false)
						this.reinitialiserSelection()
						this.reinitialiserOutils()
					} else if (donnees.role === 'participant') {
						this.desactiverDeplacement()
						window.removeEventListener('keydown', this.gererClavierAdmin, false)
						this.reinitialiserSelection()
						this.reinitialiserOutils()
					}
				} else if (donnees.admin === this.identifiant) {
					this.chargement = false
					this.utilisateurs.forEach(function (utilisateur) {
						if (utilisateur.identifiant === donnees.identifiant) {
							utilisateur.role = donnees.role
						}
					})
					this.notification = this.$t('roleUtilisateurModifie')
				}
			}.bind(this))

			this.$socket.on('attribuerroleparticipant', function (identifiant) {
				if (identifiant === this.identifiant) {
					this.role = 'participant'
					this.utilisateurs.forEach(function (utilisateur) {
						if (utilisateur.identifiant === this.identifiant) {
							utilisateur.role = 'participant'
						}
					}.bind(this))
				}
			}.bind(this))

			this.$socket.on('reinitialiser', function () {
				this.items.forEach(function (pages) {
					pages.forEach(function (item) {
						if (item.objet === 'image') {
							this.imagesASupprimer.push(item.fichier)
						}
					}.bind(this))
				}.bind(this))
				this.definirOutilPrincipal('selectionner')
				this.nom = ''
				this.objet = ''
				this.objetVerrouille = false
				this.itemsSelectionnes = []
				this.ctrl = false
				this.items = [[]]
				this.commentaires = {}
				this.historique = []
				this.positionHistorique = 0
				this.positionActuelleHistorique = 0
				this.page = 0
				this.modale = ''
				this.menu = ''
				this.transformer()
				this.notification = this.$t('tableauReinitialise')
				this.chargement = false
			}.bind(this))
		}
	}
}
